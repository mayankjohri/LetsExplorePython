#!/usr/bin/env bash
DIR=".venv"
if [ ! -d "$DIR" ];
then
	echo "Creating venv folder"
	python -m venv "${DIR}"
fi 

. ${DIR}/bin/activate
pip install -U -r requirements.txt

# jupyter nbextension install --user --py nb-js-diagrammers
# jupyter nbextension enable --user --py nb-js-diagrammers
# jupyter serverextension enable --user --py nb-js-diagrammers


# jupyter nbextension install --user --py hide_code
# jupyter nbextension enable --user --py hide_code
# jupyter serverextension enable --user --py hide_code

#pip install git+https://github.com/bollwyvl/nb-mermaid.git@103502e6
# pip install nb-mermaid
# echo "Enabling nb-js-diagrammers plugin"
# jupyter nbextension enable nb_js_diagrammers
jupyter-notebook --ip='127.0.0.1'
deactivate
