import socket

UDP_IP = "127.0.0.1"
UDP_PORT = 5050
MESSAGE = "Guten Morgan, Namaskaram !!!"

print ("UDP target IP:", UDP_IP)
print ("UDP target port:", UDP_PORT)
print( "message:", MESSAGE)

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.sendto(MESSAGE.encode(), (UDP_IP, UDP_PORT))
data = []
while True:
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes

    print(f"{data = }")
    print(f"{addr = }")
