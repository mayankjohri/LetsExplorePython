import asyncio 
import time
import random 


async def dislay(tid):
    for x in range(3):
        num = random.randint(1, 5)
        print(f"{tid} - {x} - {num}", flush=True)
        await asyncio.sleep(num)
        print(f"     - passing from {tid}", flush=True)


async def main():
    task_1 = asyncio.create_task(dislay(1))
    task_2 = asyncio.create_task(dislay(2))
    task_3 = asyncio.create_task(dislay(3))

    await task_1
    await task_2
    await task_3


asyncio.run(main())
