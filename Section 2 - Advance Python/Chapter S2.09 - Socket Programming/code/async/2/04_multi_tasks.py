import asyncio 
import time
import random 

async def dislay(tid):
    for x in range(2):
        num = random.randint(1, 5)
        print(f"{tid} - {x} - {num}", flush=True)
        await asyncio.sleep(num)
        print(f"     - passing from {tid}", flush=True)
    return tid * x


async def main():
    tasks = []
    for indx in range(1, 5):
        tasks.append(dislay(indx))
    time.sleep(3)
    print("awaiting... ")
    ret_val = await asyncio.gather(*tasks)
    print(F"{ret_val = }")
    

asyncio.run(main())
