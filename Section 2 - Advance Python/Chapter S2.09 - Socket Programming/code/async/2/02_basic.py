import asyncio 

from time import sleep, ctime

async def task_a(delay):
    for i in range(5):
        print(i)
        await asyncio.sleep(delay)

def blocking_func():
    sleep(0.4)
    print(f"{ctime()} Ending...")



loop = asyncio.get_event_loop()

task = loop.create_task(task_a(0.3))

loop.run_in_executor(None, blocking_func)
loop.run_until_complete(task)
print("At location: 2")
pending = asyncio.all_tasks(loop=loop)
# for t in pending:
#     t.cancel()
group = asyncio.gather(*pending, return_exceptions=True)
loop.run_until_complete(group)

loop.close()

