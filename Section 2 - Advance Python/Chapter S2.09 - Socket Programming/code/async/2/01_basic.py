import asyncio, time 


async def main():
    print(f"{time.ctime()}: Starting")
    await asyncio.sleep(3)
    print(f"{time.ctime()}: Ending")


loop = asyncio.get_event_loop()
task = loop.create_task(main())
loop.run_until_complete(task)

for rtask in (pending := asyncio.all_tasks(loop=loop)):
    task.cancel()

group = asyncio.gather(*pending, return_exceptions=True)
loop.run_until_complete(group)
loop.close()

