import asyncio 
import time
import random 

async def dislay(tid):
    for x in range(3):
        num = random.randint(1, 5)
        print(f"{tid} - {x} - {num}", flush=True)
        await asyncio.sleep(num)
        print(f"     - passing from {tid}", flush=True)
    return tid * x

async def main():
    tasks = []
    for indx in range(1, 5):
        tasks.append(asyncio.create_task(dislay(indx)))
    result = await asyncio.gather(*tasks)
    print(f"{result = }")
    

asyncio.run(main())
