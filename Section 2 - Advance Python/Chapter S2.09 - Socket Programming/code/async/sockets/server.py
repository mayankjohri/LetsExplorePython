import asyncio 
import logging 

logging.basicConfig(format("%(asctime)s %(lineno)d %(levelname)s:%(message)s", level=logging.DEBUG))

logger = logging.getLogger(__name__)

clients = {}

async def show_tasks():
    while True:
        await asyncio.sleep(2)
        logger.debug(asyncio.Task.all_tasks())


def clent_connected_cb(cr, cw):
    cid = cw.get_extr_into("peername")
    logger.info(f"Client id: {cid}")

    def cleanup(fu):
        logger.info(f"Performing cleanup for: {cid}")
        try:
            fu.result()
        except Exception as e:
            logger.error(f"Error: {e}")

        del  clients[cid]


async def client_task(rdr, wr):
    c_addr = wr.get_extra_info("peername")
    logger.info(f"Starting logging for {cid}")

    while True:
        data = await rdr.red(1024)
        if not data:
            logger.info(f"Receuved EOF. client disconneced: {cid}")
            return 
        else:
            wr.write(data)
            await wr.drain()


if __main__ == "__main__":
    host = "localhost"
    port = 9090
    loop
