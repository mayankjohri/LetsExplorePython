import asyncio

async def tcp_echo_client(message):
    reader, writer = await asyncio.open_connection(
        '127.0.0.1', 5000)

    print(f'Send: {message!r}')
    writer.write(message.encode())
    await writer.drain()

    data = await reader.read(100)
    print(f'Received: {data.decode()!r}')

    print('Close the connection')
    writer.close()
    await writer.wait_closed()


async def main():
    tasks = []
    for indx in range(5):
        tasks.append(tcp_echo_client(f"Hello from {indx}"))

    await asyncio.gather(*tasks)



asyncio.run(main())

