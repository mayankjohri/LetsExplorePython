import asyncio


async def echo(reader, writer):
    while True:
        data = await reader.read(1024)
        print(f"Got data: {data}")

        if not data:
            writer.close()
            break
        writer.write(data)
        await writer.drain()

    writer.close()



async def main(host, port):

    server  = await asyncio.start_server(echo, host, port, backlog=10)
    await server.serve_forever()


asyncio.run(main("127.0.0.1", 5050))
