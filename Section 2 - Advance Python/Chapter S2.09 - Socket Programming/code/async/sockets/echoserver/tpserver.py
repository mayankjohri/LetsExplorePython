import asyncio

TIMEOUT = 1  # Second


class TimeoutServer(asyncio.Protocol):    
    def __init__(self):
        loop = asyncio.get_running_loop()
        self.timeout_handle = loop.call_later(
            TIMEOUT, self._timeout,
        )    

    def connection_made(self, transport):
        self.transport = transport    

    def data_received(self, data):
        self.timeout_handle.cancel()    
    
    def _timeout(self):
        self.transport.close()



async def main(host, port):
    loop = asyncio.get_running_loop()
    server = await loop.create_server(TimeoutServer, host, port)
    await server.serve_forever()


asyncio.run(main('127.0.0.1', 5000))
