import asyncio

class EchoProtocol(asyncio.Protocol):
    # def __init__(self, message):
    #    self.message = message 

    def connection_made(self, transport):
        print(dir(transport))
        # transport.write(self.message)
        self.transport = transport    
    
    def connection_lost(self, transport):
        print("Connection lost")

    def data_received(self, data):
        print(f"Received data: {data}")
        self.transport.write(data)


    
async def main(host, port):

    loop = asyncio.get_running_loop()
    server = await loop.create_server(EchoProtocol, 
                                      host,
                                      port)
    await server.serve_forever()


asyncio.run(main('127.0.0.1', 5000))
