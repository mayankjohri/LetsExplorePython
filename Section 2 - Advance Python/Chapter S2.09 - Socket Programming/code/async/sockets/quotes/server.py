from random import randint
import asyncio

def get_quotes():

    with open("quotes.txt") as fp:
        quotes = fp.readlines()
    return quotes

def get_quote():
    return bytes(quotes[randint(0,size)],  'utf-8')


async def quote_server(reader, writer):
    while True:
        data = get_quote()
        writer.write(data)
    
        await writer.drain()
        writer.close()


async def main(host, port):
    server = await asyncio.start_server(quote_server, host, port)
    await  server.serve_forever()


if __name__ == "__main__":
    quotes = get_quotes()
    size = len(quotes) - 1
    asyncio.run(main('127.0.0.1', 5001))
