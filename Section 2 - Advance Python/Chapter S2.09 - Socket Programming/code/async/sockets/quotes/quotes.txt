People say that they understand what I say and I am simple. I am not simple, I am clear
The highest form of service is to help a person who is incapable of thanking in return
A value is valuable when the value of value is valuable to oneself
A person who consumes the least and contributes most is a mature person, for in giving lies the self-growth
Give the world the best you have and best will come back to you
You want to change others so that you can be free. But, it never works that way. Accept others and you are free
Validation of culture of a child is itself validation of a child itself
Enlighten- It cannot be an event. All that is here is non-duality. How will it happen? It is clarity
It is not wrong to be an ignorant; it is an error to continue to be an ignorant
You cannot stand tall emotionally when you are always at the receiving end
Whatever kind of seed is sown in a field, prepared in due season, a plant of that same kind, marked with the peculiar qualities of the seed, springs up in it
