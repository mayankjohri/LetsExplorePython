import asyncio
from time import ctime

async def Suprabhat():
    print(f'Guten Morgan - {ctime()}')
    await asyncio.sleep(4)
    print(f'means Suprabhat - {ctime()}')
    await asyncio.sleep(1)
    print(f'in German Language {ctime()}')

asyncio.run(Suprabhat())
