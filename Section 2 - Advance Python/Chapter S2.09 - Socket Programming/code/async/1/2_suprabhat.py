import asyncio
from time import ctime


async def printme(msg, duration):
    print(f'1. {msg} - {ctime()}')
    await asyncio.sleep(duration)
    print(f'  2. {msg} - {ctime()}')


async def Suprabhat():
    t1 = asyncio.create_task(printme("Guten Morgan", 4))
    t2 = asyncio.create_task(printme('means Suprabhat', 1))
    t3 = asyncio.create_task(printme('in German Language', 2))
    await t1
    await t2
    await t3


asyncio.run(Suprabhat())
