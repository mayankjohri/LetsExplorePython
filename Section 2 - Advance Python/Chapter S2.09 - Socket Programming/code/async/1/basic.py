import asyncio 
from time import ctime

async def task_a(delay, tid):
    for i in range(5):
        print(f"{tid}: {i}")
        await asyncio.sleep(delay)

def stopme(loop):
    print(ctime())
    loop.stop()
    print(ctime())

loop = asyncio.get_event_loop()

task = loop.create_task(task_a(0.3, 1), task_a(0.8, 2))
# loop.call_later(15, stopme, loop)

loop.run_until_complete(task)
loop.close()

