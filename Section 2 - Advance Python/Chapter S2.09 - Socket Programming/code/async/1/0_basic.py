import asyncio, time 


async def main():
    print(f"{time.ctime()}: Starting")
    await asyncio.sleep(3)
    print(f"{time.ctime()}: Ending")

asyncio.run(main())

