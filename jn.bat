@echo on
@break on
@color 0a
@cls


SET DIR=C:\\venv
IF not exist "%DIR%" (
  echo "Creating venv folder"
  python -m venv "%DIR%"
)
CALL "%DIR%\Scripts\activate.bat"
pip install -U -r requirements.txt
jupyter-notebook "--ip=127.0.0.1"
deactivate