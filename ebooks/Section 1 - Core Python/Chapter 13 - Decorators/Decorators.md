# Decorators

> **Note**:
>
> Read this chapter after reading basics of Functional Programming 

A decorator is the name used for a software design pattern. They can dynamically alter the functionality of a function, method, or class without having to directly use subclasses or change the source code of the function being decorated.

Python decorator is a specific change to the Python syntax that allows us to more conveniently alter functions and methods (and possibly classes in a future version). This supports more readable applications of the Decorator Pattern but also other uses as well.

Lets look at the below example.


```python
def bread(test_funct):
    def wheat_bread(filling=""):
        print(r"</''''''\>")
        test_funct(filling)
        print(r"<\______/>")
    return wheat_bread

def ingredients(test_funct):
    def fillings(ingred=""):
        print("#tomatoes#")
        test_funct(ingred)
        print("~salad~")
    return fillings


def cheese(food="--Say Cheese--"):
    print(food)
```

In the above example, we have two functions `bread` and `ingredients` which have inner functions and a third normal function whose functionality we will change.

In both `bread` and `ingredients` we can pass another function as a parameter and in the below code we are going to pass `cheese` to both the functions. 


```python
bread_cheese = bread(cheese)
print(f"{bread_cheese = }")
```

    bread_cheese = <function bread.<locals>.wheat_bread at 0x7fe5a0589760>



```python

bread_cheese_1 = bread(cheese)
print(f"{bread_cheese_1 = }")
```

    bread_cheese_1 = <function bread.<locals>.wheat_bread at 0x7fe5a05896c0>


Passing `cheese` to function `bread` as a parameter returned the inner functon `wheat_bread` with `test_funct` pointing to `cheese` function. 

Now if we call the returned function with some data `Egg Plant` in our case, it will first execute the `test_funct` which internally will call `cheese` function as shown below.


```python
bread_cheese("Egg Plant")
```

    </''''''\>
    Egg Plant
    <\______/>



```python
inn = bread(ingredients(cheese))
```


```python
inn("Potato Chips")
```

    </''''''\>
    #tomatoes#
    Potato Chips
    ~salad~
    <\______/>



```python
inn = ingredients(bread(cheese))
inn("Potato Chips")
```

    #tomatoes#
    </''''''\>
    Potato Chips
    <\______/>
    ~salad~



```python
inn = bread(ingredients(bread(cheese)))
inn("Potato Chips")
```

    </''''''\>
    #tomatoes#
    </''''''\>
    Potato Chips
    <\______/>
    ~salad~
    <\______/>


## Function Decorators

A function decorator is applied to a function definition by placing it on the line before that function definition begins


```python
@bread
@ingredients
def sandwich(food="--Say Cheese--"):
    print(food)

sandwich("Potato Chips")
```

    </''''''\>
    #tomatoes#
    Potato Chips
    ~salad~
    <\______/>


> ***!!! Order Matters !!!*** 
>
> Order in which we place our decorators matter as shown in below examples.


```python
@ingredients
@bread
def sandwich(food="--Say Cheese--"):
    print(food)

sandwich("Kashimiri Mirch")
```

    #tomatoes#
    </''''''\>
    Kashimiri Mirch
    <\______/>
    ~salad~



```python
def Jam(food="Jam"):
    print(food)

Jam()
```

    Jam



```python
@bread
@ingredients
def Jam(food):
    print(food)

Jam("Jam")
```

    </''''''\>
    #tomatoes#
    Jam
    ~salad~
    <\______/>


So, my `Jam` is doing more than what originally it was designed to do. 


```python
def sandwich(food="--Say Cheese--"):
    print(food)

sandwich("Yummy")
```

    Yummy



```python
# By omiting the `inner_funct in `inner`, I am making sure that my decorated 
# function is very called. 

def diet_sandwitch(_):
    def inner(c):
        ...  # Using Ellipsis
    return inner

@bread
@diet_sandwitch
@ingredients
def sandwich(food="--Say Cheese--"):
    print(food)

sandwich("Yummy")
```

    </''''''\>
    <\______/>



```python
# Re-adding the calling of `inner_func` in `inner`.

def diet_sandwitch(inner_func):
    print("inside diet_sandwitch")
    def inner(c):
        print("salad")
        inner_func(c)
    return inner

@bread
@diet_sandwitch
@ingredients
def sandwich(food="--Say Cheese--"):
    print(food)
```

    inside diet_sandwitch



```python
sandwich("Yummy")
```

    </''''''\>
    salad
    #tomatoes#
    Yummy
    ~salad~
    <\______/>



```python
# def onexit(f):
#     import atexit
#     atexit.register(f)
#     print("in onexit")
#     return f

# @onexit
# def sandwich(food="--Say Cheese--"):
#     print(food)
    
# sandwich("Test")
```

    in onexit
    Test


## Decorators with arguments

### Using Functions

We can create a function which can take an argument and wrap the original decorator inside as shown in below code sample.


```python
def run_me(count=1):
    """Level 1"""
    print("Level 1 Start")
    print(f".{count = }")
    
    def __inner__(func):
        """Level 2"""
        print("Level 2 Start")
        print(f".. {count = }")
        for _ in range(count):
            func()
        print("Level 2 Ends")
        
    print("Level 1 Ends")
    return __inner__
```


```python
@run_me(3)
def print_args():
    """ print_args function"""
    print("TEST")
```

    Level 1 Start
    .count = 3
    Level 1 Ends
    Level 2 Start
    .. count = 3
    TEST
    TEST
    TEST
    Level 2 Ends



```python
# try:
#     print(f"{print_args.__name__ = }")
#     print(f"{print_args.__doc__ = }")
# except Exception as e:
#     print(e)
```


```python
def decorator_factory(*argument):
    def decorator(function):
        def wrapper(*args, **kwargs):
            print("Inside wrapper...Before")
            print(f"{argument = },\n{args = },\n{kwargs = }")
            result = function(*args, **kwargs)
            print("Inside wrapper...After")
            return result
        return wrapper
    return decorator


@decorator_factory("arg1")
def print_args(*args, **kwargs):
    print("TEST")
    for arg in args:
        print("arg", arg)
    for k, v in kwargs.items():
        print("kwargs", k, v)
```


```python
print_args(1, 2 , 3, test=11)
```

    Inside wrapper...Before
    argument = ('arg1',),
    args = (1, 2, 3),
    kwargs = {'test': 11}
    TEST
    arg 1
    arg 2
    arg 3
    kwargs test 11
    Inside wrapper...After


Level 1: Decorator Arguments

Level 2: Function 

Level 3: Function Call Arguments


```python
def corps_fun(*deco_args, **deco_kwargs):
    """Level 1"""
    print("Level 1 Start")
    
    def decorator(func):
        """Level 2"""
        print("Level 2 Start")

        def inner(*args, **kwargs):
            """Level 3"""
            print("Level 3 Start")
            print("inside inner args:", deco_args)
            for k, v in deco_kwargs.items():
                print("inside inner kwars:", k, v)
            print("deco_kwargs:", deco_kwargs)
            print(*args, kwargs)
            result = func(*args, **kwargs)
            print("Level 3 Ends")
            return result
        print(f"Level 2 Ends - {inner}")
        return inner
    print("Level 1 Ends")
    return decorator


@corps_fun("arg1", "arg2")
def print_args(*args, **kwargs):
    print("TEST")
    for arg in args:
        print("arg", arg)
    for k, v in kwargs.items():
        print("kwargs", k, v)
```

    Level 1 Start
    Level 1 Ends
    Level 2 Start
    Level 2 Ends - <function corps_fun.<locals>.decorator.<locals>.inner at 0x7fca3d0916c0>



```python
print_args(1, 2, 3, corp="Bhopal Municipal Corporation")
```

    Level 3 Start
    inside inner args: ('arg1', 'arg2')
    deco_kwargs: {}
    1 2 3 {'corp': 'Bhopal Municipal Corporation'}
    TEST
    arg 1
    arg 2
    arg 3
    kwargs corp Bhopal Municipal Corporation
    Level 3 Ends



```python
def decorator(*deco_args, **deco_kwargs):
    print(f"{deco_args=}, {deco_kwargs=}")
    
    def real_decorator(func):
        print(f"{func=}")
        
        def inner(*args, **kwargs):
            print(f"inside inner args: {args = }")
            print(f"inside inner kwargs: {kwargs = }")
            # Calling the actual function
            func(*args, **kwargs)
        return inner
    return real_decorator


@decorator("arg1", "arg2")
def print_args(*args, **kwargs):
    print(".TEST")
    for arg in args:
        print(".arg", arg)
    for k, v in kwargs.items():
        print(".kwargs", k, v)
```

    deco_args=('arg1', 'arg2'), deco_kwargs={}
    func=<function print_args at 0x7fca3d091870>



```python
print_args(1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: args = (1, 2, 3)
    inside inner kwargs: kwargs = {'corp': 'Bhopal Municipal Corporation'}
    .TEST
    .arg 1
    .arg 2
    .arg 3
    .kwargs corp Bhopal Municipal Corporation



```python
@decorator("arg1", "arg2", ver="10.2.301")
def print_args(*args, **kwargs):
    print("TEST")
    for arg in args:
        print("arg", arg)
    for k, v in kwargs.items():
        print("kwargs", k, v)
```

    deco_args=('arg1', 'arg2'), deco_kwargs={'ver': '10.2.301'}
    func=<function print_args at 0x7fca3d091ab0>



```python
print_args(1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: (1, 2, 3)
    inside inner kwargs: {'corp': 'Bhopal Municipal Corporation'}
    TEST
    arg 1
    arg 2
    arg 3
    kwargs corp Bhopal Municipal Corporation



```python
def decorator(*deco_args, **deco_kwargs):
    def real_decorator(func):
        def inner(*args, **kwargs):
            print("inside inner args:", args)
            print("inside inner kwargs:", kwargs)
            # Calling the actual function
            if args[0] == 0:
                func(*args, **kwargs)
        return inner
    return real_decorator


@decorator("arg1", "arg2")
def print_args(*args, **kwargs):
    print("TEST")
    for arg in args:
        print("arg", arg)
    for k, v in kwargs.items():
        print("kwargs", k, v)
```


```python
print_args(1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: (1, 2, 3)
    inside inner kwargs: {'corp': 'Bhopal Municipal Corporation'}



```python
print_args(0, 1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: (0, 1, 2, 3)
    inside inner kwargs: {'corp': 'Bhopal Municipal Corporation'}
    TEST
    arg 0
    arg 1
    arg 2
    arg 3
    kwargs corp Bhopal Municipal Corporation


In the above example, we bypassed the execution of actual function itself.

Python provides `wrapper` function of `functools` in order to ease the debugging of decorators, thus its adviced to use them while creating the decorators.


```python
from functools import wraps

def decorator(*deco_args, **deco_kwargs):
    method_or_name = deco_args
    
    def real_decorator(method):    
        if callable(method_or_name):
            print("if", method_or_name)
            method.gw_method = method.__name__
        else:
            print("Else", method_or_name)
            method.gw_method = method_or_name

        @wraps(method)
        def wrapper(*args, **kwargs):
            method(*args, **kwargs)
        return wrapper
    
    if callable(method_or_name):
        return real_decorator(method_or_name)
    return real_decorator

@decorator("arg1", "arg2", test="test")
def print_args(*args, **kwargs):
    """
    Custom Print Function 
    """
    for arg in args:
        print(f"{arg = }")
    for k, v in kwargs.items():
        print(f"{k = }, {v = }")

print_args(1, 2, 3, d="Satyam")
```

    Else ('arg1', 'arg2')
    arg = 1
    arg = 2
    arg = 3
    k = 'd', v = 'Satyam'



```python
print(f"{print_args.__name__ = }")
print(f"{print_args.__doc__ = }")
```

    print_args.__name__ = 'print_args'
    print_args.__doc__ = '\n    Custom Print Function \n    '


#### Conditional Decorators

There are scenarios were we want to run the decorators only under certain condition, although Python do not natively support it, but we can achieve it with the following code template.


```python
def condition(**deco_kwargs):
    
    def __decorator__(func):
    
        def __inner__(*args, **kwargs):
            if deco_kwargs.get('flag', False):
                print("Flag found")
                return func(*args, **kwargs)
            else:
                print("Flag not found")
            
        return __inner__
    return __decorator__
        

def decorator(func):
    def inner(*args, **kwargs):
        print("Yadayada _ in inner")
        print(*args, **kwargs)
        print(args)
        for k, v in kwargs.items():
            print(k, v)
        func(*args, **kwargs)
    return inner

debug = False

@condition(flag=debug)
@decorator
def print_args(*args, **kwargs):
    for arg in args:
        print(arg)
    for k, v in kwargs.items():
        print(k, v)


print_args(1, 2)
print("...done....")
```

    Flag not found
    ...done....


In the above example, any decorator below `condition` will not run, but if there were any decorators above it, they will still get partially executed. 


```python
debug = True

def condition(**deco_kwargs):
    def __decorator__(func):
        def __inner__(*args, **kwargs):
            if deco_kwargs.get('flag', False):
                return func(*args, **kwargs)
        return __inner__
    return __decorator__
        

def decorator(func):
    def inner(*args, **kwargs):
        print("Yadayada _ in inner")
        print(*args, **kwargs)
        print(args)
        for k, v in kwargs.items():
            print(k, v)
        func(*args, **kwargs)
    return inner

@condition(flag=debug)
@decorator
def print_args(*args, **kwargs):
    for arg in args:
        print(arg)
    for k, v in kwargs.items():
        print(k, v)


print_args(1, 2)
```

    Yadayada _ in inner
    1 2
    (1, 2)
    1
    2


### Context Managers as Decorators 

Context managers can also be used as decorators, as shown below (https://docs.python.org/dev/library/contextlib.html#contextlib.ContextDecorator)


```python
from contextlib import ContextDecorator

class mycontext(ContextDecorator):
    def __enter__(self):
        print('Starting')
        return self

    def __exit__(self, *exc):
        print('Finishing')
        return False

@mycontext()
def function():
    print('The bit in the middle')
```


```python
function()
```

    Starting
    The bit in the middle
    Finishing



```python
def function_2():
    print("This is function 2")
    
with mycontext() as t: 
    function_2()
```

    Starting
    This is function 2
    Finishing



```python
from contextlib import ContextDecorator

class mycontext(ContextDecorator):
    def __enter__(self):
        print('Starting')
        return self

    def __exit__(self, *exc):
        print('Finishing')
        return False

@mycontext()
def function():
    raise Exception("Raising Error")
    print('The bit in the middle')

try:
    function()
except Exception as e:
    print("Error:", e)
```

    Starting
    Finishing
    Error: Raising Error


In the above example, although the exception was not handled, yet, `__end__` funtion was called by python

## Decorators & Classes

We can apply decorators on classes in two ways.

- Decorator(s) on methods
- Decorator(s) on classes

Before we discuss it in details, a bit of understanding is in order. 

So Lets begin

### Decorator(s) on methods

#### Bound methods

Unless you tell it not to, Python will create what is called a bound method when a function is an attribute of a class and you access it via an instance of a class. This may sound complicated but it does exactly what you want.


```python
class A(object):
    def method(*argv):
        return argv
a = A()
a.method
```




    <bound method A.method of <__main__.A object at 0x7f3f1a7499c0>>




```python
a.method('an arg')
```




    ('an arg',)



#### Examples of decorators on methods

##### staticmethod()

A static method is a way of suppressing the creation of a bound method when accessing a function. Please note that we are using `staticmethod` decorator in class `A` method `method` 


```python
class A(object):
    @staticmethod
    def method(*argv):
        return argv
a = A()
a.method
```




    <function __main__.A.method(*argv)>



When we call a static method we don’t get any additional arguments.


```python
a.method('an arg')
```




    ('an arg',)



##### classmethod

A class method is like a bound method except that the class of the instance is passed as an argument rather than the instance itself. We are using `classmethod` decorator in class `A` method `method`  


```python
class A(object):
    
    @classmethod
    def method(*argv):
        return argv
a = A()
a.method
```




    <bound method A.method of <class '__main__.A'>>




```python
a.method('an arg')
```




    (__main__.A, 'an arg')




```python
def test(strg):
    print("Name: ", strg)
    
def hello(func, name):
    print("Ja")
    func(name)
    
hello(test, "Mayank")
```

    Ja
    Name:  Mayank



```python
class B(object):
    @classmethod
    def method(*argv):
        return argv
```


```python
a = B()
print(f"{a.method() = }")
```

    a.method() = (<class '__main__.B'>,)



```python
# from functools import wraps

def debug(func):

    def __wrapper__(args):
        print(f"Debug: Starting function {func}")
        ret = func(args)
        print(f"""Debug:  Function {func} execution ended 
        with return value {ret}.""")
        return ret
    
    return __wrapper__

class Runner():
    
    @debug
    def run_me(self):
        print("Running run_me")
        return 10
    
mayank = Runner()
mayank.run_me()

```

    Debug: Starting function <function Runner.run_me at 0x7fe5a058a980>
    Running run_me
    Debug:  Function <function Runner.run_me at 0x7fe5a058a980> execution ended 
            with return value 10.





    10



We can make the above code a bit more versatile, by adding arguemnts to it.


```python
from functools import wraps

def debug(func):
    
    @wraps(func)
    def __wrapper__(*args, **kargs):
        print(f"Debug: Starting function {func} with {args = } & {kargs = }")
        ret = func(*args, **kargs )
        print(f"""Debug:  Function {func} execution ended 
        with return value {ret}.""")
        return ret
    
    return __wrapper__

class Runner():
    
    @debug
    def run_me(self, name):
        print("Running run_me")
        return 10
    
mayank = Runner()
mayank.run_me("Mayank")

```

    Debug: Starting function <function Runner.run_me at 0x7fe5a058b060> with args = (<__main__.Runner object at 0x7fe5a05bbc80>, 'Mayank') & kargs = {}
    Running run_me
    Debug:  Function <function Runner.run_me at 0x7fe5a058b060> execution ended 
            with return value 10.





    10




```python
# For next version - TODO
# from functools import wraps
# from timeit import timeit

# def debug(func):
    
#     @wraps(func)
#     def __wrapper__(*args, **kargs):
#         print(f"Debug: Starting function {func} with {args = } & {kargs = }")
#         calling_func = f"{func.__name__}" + "(*args, **kargs )"
#         timeit(calling_func, globals=globals())
#         print(calling_func)
#         ret = 0
# #         ret = func(*args, **kargs)
# #         print(f"""Debug:  Function {func} execution ended 
# #         with return value {ret}.""")
#         return ret
    
#     return __wrapper__

# class Runner():
    
#     @debug
#     def run_me(self, name):
#         print("Running run_me")
#         return 10
    
# mayank = Runner()
# mayank.run_me("Mayank")

```

#### Decorators on Classes

Lets create a decorator which can be used to create a simple singleton class


```python
def singleton(cls):
    instances = None

    def getinstance():
        nonlocal instances
        
        if not instances:
            instances = cls()
        print(f"{instances = }")
        return instances
    
    return getinstance

@singleton
class GodMade(object):
    def __init__(self):
        pass
```


```python
a = GodMade()
b = GodMade()

print(f"{a = }, {b = }")
print(f"{a is b = }")
```

    instances = <__main__.GodMade object at 0x7fe5a05b9940>
    instances = <__main__.GodMade object at 0x7fe5a05b9940>
    a = <__main__.GodMade object at 0x7fe5a05b9940>, b = <__main__.GodMade object at 0x7fe5a05b9940>
    a is b = True



```python
@singleton
class MadeGod(object):
    def __init__(self):
        pass

d = MadeGod()
e = MadeGod()
```

    instances = <__main__.MadeGod object at 0x7fca3d07b070>
    instances = <__main__.MadeGod object at 0x7fca3d07b070>



```python
print(f"{a is d = } and {b is d = }")
```

    a is d = False and b is d = False


## References:

- https://www.python.org/dev/peps/pep-0318/
