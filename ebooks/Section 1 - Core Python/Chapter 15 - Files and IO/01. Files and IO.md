# Files and I/O

## Files

Python provides many modules to handle files, depending upon it's property. We are going to cover few of them in this section. 



```python
# The old way .... 
# The C, C++ etc way
# Please do not use it in Production code.

import sys

# Create an object of type file
# 'r' -> Read Mode
# 'w' -> Write Mode
# 'a' -> Append Mode

temp = open('temp.txt', 'w')

# Write output
for i in range(7):
    temp.write('%03d\n' % i)

temp.close()

# default mode is read only. 
temp = open('temp.txt')
# temp1 opening in write mode 
temp1 = open('temp1.txt', "w")

# Write in terminal
# Reading a line at a time from temp.txt file
# and writing it in temp1.txt file
for x in temp:
    print("x = {x}".format(x=x), end="")
    temp1.write(x)

temp.close()
temp1.close()
```

    x = 000
    x = 001
    x = 002
    x = 003
    x = 004
    x = 005
    x = 006



```python
# New Way, The Python way
# Using context manager `with`

# Create an object of type file
# equivalent: temp = open(...)
with open('temp.txt', 'w') as temp:
    # Write output
    for i in range(5):
        temp.write('%04d\n' % i)

with open('temp.txt') as temp:
    # lets read the file
    for x in temp:
        x = x.strip()   # Stripping the newline at the end of the string
        print(x, end=", ")  # Write in terminal
        
print("\b\b")
```

    0000, 0001, 0002, 0003, 0004, 



```python
# replace content of the file without temp file. 
# on the fly content changes
# NEVER EVER DO THIS
txt = []
temp = open('temp.txt', 'r')
for t in temp.readlines():
#     print("d")
    txt.append(t.replace('0','11'))

temp.close()
with open('temp.txt', 'w') as tfile:
    tfile.writelines(txt)
```


```python
# copying content from temp.txt to temp2.txt,
# Bad for large files 

with open('temp.txt', 'r') as temp:
    with open('temp2.txt', 'w') as tfile:
        tfile.writelines(temp.readlines())
```


```python
# copying content from temp.txt to temp2.txt,
# Dont try on big files

with open('temp.txt') as temp:
    with open('temp3.txt', 'w') as tfile:
        for content in temp.readlines():
            print(content, end="")
            tfile.write(content)
```

    0000
    0001
    0002
    0003
    0004


We can club both the context managers `with` as shown in the below example


```python
# copying content from temp.txt to temp2.txt
# Dont try on big files 

with open('temp.txt') as temp, \
     open('temp4.txt', 'w')  as tfile:
    for content in temp.readlines():
        print(content, end="")
        tfile.write(content)
```

    0000
    0001
    0002
    0003
    0004


If we have to copy only few top lines from file, than something similar to below example can be used. In it we are using optional parameter `size` to tell how much data needs to be copied. 

> Note
> <hr>
> If `size` is provided, then it will be increased to accomodate full line as shown in below example.


```python
with open('temp.txt') as temp, open('temp4.txt', 'w')  as tfile:
    for content in temp.readlines(8):
        print(f"{content=}", end="")
        tfile.write(content)
```

    content='0000\n'content='0001\n'

We have returned 12 charactes instead of 10,


```python
for s in range(8, 13):
    print("~" * 20, "size is:", s)
    with open('temp.txt') as temp, \
         open('temp4.txt', 'w')  as tfile:
        for content in temp.readlines(s):
            print(content, end="")
            tfile.write(content)
```

    ~~~~~~~~~~~~~~~~~~~~ size is: 8
    0000
    0001
    ~~~~~~~~~~~~~~~~~~~~ size is: 9
    0000
    0001
    ~~~~~~~~~~~~~~~~~~~~ size is: 10
    0000
    0001
    0002
    ~~~~~~~~~~~~~~~~~~~~ size is: 11
    0000
    0001
    0002
    ~~~~~~~~~~~~~~~~~~~~ size is: 12
    0000
    0001
    0002


We should not use `readlines` when processing large files, but code similar to below.


```python
import os 
os.remove('temp4.txt')

with open('temp.txt') as temp, \
     open('temp4.txt', 'w')  as tfile:
    for content in temp:
        print(content, end="")
        tfile.writelines(content)
```

    0000
    0001
    0002
    0003
    0004



```python
# import os 
# os.remove('temp4.txt')

# with open('temp.txt') as temp, \
#      open('temp4.txt', 'w')  as tfile:
#      for content in temp[:2]:
#         print(content, end="")
#         tfile.writelines(content)
```

    0000
    0001
    0002
    0003
    0004



```python
import os 
os.remove('temp4.txt')

with open('temp.txt') as temp, open('temp4.txt', 'w')  as tfile:
    count = 0
    for content in temp:
        if count >= 3:
            break
        count += 1
        print(content, end="")
        tfile.writelines(content)
```

    0000
    0001
    0002



```python
import os 
os.remove('temp4.txt')

with open('temp.txt') as temp, open('temp4.txt', 'w')  as tfile:
    count = 0
    for indx, content in enumerate(temp):
        if count >= 3:
            break
        count += 1
        print(indx, content, end="")
        tfile.writelines(content)
```

    0 0000
    1 0001
    2 0002



```python
# Prints a list with all the lines from a file
print (open('temp4.txt').readlines())
```

    ['0000\n', '0001\n', '0002\n']



```python
import sys
import os.path

fn = 'temp.txt'

if os.path.exists(fn):
    # Numbering lines
    for i, s in enumerate(open(fn), start=1):
        print (i, s, end="")
else:
    print("Sorry, I was not able to locate the file.")
```

    1 0000
    2 0001
    3 0002
    4 0003
    5 0004


It is possible to read all the lines with the method `readlines()`:


```python
print(len(open('temp.txt').readlines())) # 20
print(len(open('temp.txt').read())) # 4x20
```

    5
    25


File Systems
-------------------
Modern operating systems store files in hierarchical structures called *file systems*.

Several features related to file systems are implemented in the module *os.path*, such as: 

+ `os.path.basename()`: returns the final component of a path.
+ `os.path.dirname()`: returns a path without the final component.
+ `os.path.exists()`: returns *True* if the path exists or *False* otherwise.
+ `os.path.getsize()`: returns the size of the file in *bytes*.
- `os.path.join()`: joins the files, directory

*glob* is another module related to the file system:


```python
print(dir(os.path))
```

    ['__all__', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '_get_sep', '_joinrealpath', '_varprog', '_varprogb', 'abspath', 'altsep', 'basename', 'commonpath', 'commonprefix', 'curdir', 'defpath', 'devnull', 'dirname', 'exists', 'expanduser', 'expandvars', 'extsep', 'genericpath', 'getatime', 'getctime', 'getmtime', 'getsize', 'isabs', 'isdir', 'isfile', 'islink', 'ismount', 'join', 'lexists', 'normcase', 'normpath', 'os', 'pardir', 'pathsep', 'realpath', 'relpath', 'samefile', 'sameopenfile', 'samestat', 'sep', 'split', 'splitdrive', 'splitext', 'stat', 'supports_unicode_filenames', 'sys']



```python
# Bad code example

import os.path 

a = r"/home/mayank/apps/appvhelper"
b = r"conf/myprogram.conf"
c = os.path.join(a, b)
print(c)
```

    /home/mayank/apps/appvhelper/conf/myprogram.conf



```python
import os.path 

a = "/", "home", "mayank", "apps","appvhelper"
b = r"myprogram.conf"

c = os.path.join(*a, b)
print(c)
```

    /home/mayank/apps/appvhelper/myprogram.conf


Based on the OS the "\\" or "/" is used to join the directories.


```python
# Gotcha's
import os.path 

conf_files = [r"/home/mayank/apps/ls/warzone",
              r"C:\Users\God\mayaappvhelper\conf"
             ]
file_name = "user.conf"
for conf in conf_files:
    print(os.path.join(conf, file_name))
```

    /home/mayank/apps/ls/warzone/user.conf
    C:\Users\God\mayaappvhelper\conf/user.conf



```python
# Better code.
# For *inx and macOS

db_file = os.path.join("/", "home", "user","database", "db.sqlite")
print(db_file)
```

    /home/user/database/db.sqlite






```python
# For ReactOS or Windows

db_file = os.path.join("c:", "home", "user","database","db.sqlite")
print(db_file)
```

    c:/home/user/database/db.sqlite



```python
base_folder = "/"
# base_folder = "c:\\"

db_file = os.path.join(base_folder, "home", "user","database","db.sqlite")
print(db_file)
```

    /home/user/database/db.sqlite



```python
# Gotcha's
import os.path 

conf_files = [r"/home/mayank/apps/ls/warzone/myprogram.conf",
              r"C:\Users\God\mayaappvhelper\conf\user.conf"
             ]

for conf in conf_files:
    print(os.path.split(conf))
```

    ('/home/mayank/apps/ls/warzone', 'myprogram.conf')
    ('', 'C:\\Users\\God\\mayaappvhelper\\conf\\user.conf')


Since the above code was exected on "Linux", thus the second conf_file was not properly split. we might use `ntpath` instead as shown below to forcefully use windows specific changes. 


```python
# Forcing to use Windows File seperators.

import ntpath

cp = r"C:\Users\God\mayaappvhelper\conf\user.conf"
print(ntpath.split(cp))
```

    ('C:\\Users\\God\\mayaappvhelper\\conf', 'user.conf')



```python
# The seperator is not added to drive letter

db_file = ntpath.join("c:", "appfolder", "config","database","db.sqlite")
print(db_file)
```

    c:appfolder\config\database\db.sqlite



```python
db_file = ntpath.join("c:\\", "appfolder", "config","database","db.sqlite")
print(db_file)
```

    c:\appfolder\config\database\db.sqlite



```python
# A better approch
import os.path 

conf_files = [
    ("home", "mayank", "app", "myprogram.conf"),
    ("C:", "Users", "God", "user.conf")
]

for conf in conf_files:
    print(os.path.join(*conf))
```

    home/mayank/app/myprogram.conf
    C:/Users/God/user.conf



```python
conf_file = "appfolder\config\database\db.sqlite"
```


```python
print(os.path.split(conf_file))
```

    ('', 'appfolder\\config\\database\\db.sqlite')



```python

print(os.path.split(os.path.split(conf_file)[0]))
```

    ('', '')



```python

print(os.path.split(os.path.split(conf_file)[1]))
```

    ('', 'appfolder\\config\\database\\db.sqlite')



```python
print(os.path.splitext(conf_file))
```

    ('appfolder\\config\\database\\db', '.sqlite')



```python
print(os.path.splitext(conf_file)[0] + ".json")
```

    appfolder\config\database\db.json


Please, dont use splitext to find the file ext, you can use the following code.


```python
print(os.path.splitext(conf_file)[1].endswith(".conf"))
```

    False



```python
print(os.path.splitext(conf_file)[1].endswith(".json"))
```

    False



```python
print(conf_file.endswith(".conf"))
```

    False



```python
conf_file_win = "C:\\apps\MLCA\\test.txt"
print(os.path.splitdrive(conf_file_win))
```

    ('', 'C:\\apps\\MLCA\\test.txt')


    <>:1: SyntaxWarning: invalid escape sequence '\M'
    <>:1: SyntaxWarning: invalid escape sequence '\M'
    /tmp/ipykernel_10982/3274591370.py:1: SyntaxWarning: invalid escape sequence '\M'
      conf_file_win = "C:\\apps\MLCA\\test.txt"


> **NOTE**:
> 
> splitdrive, is good for windows based os only as other OSes dont have drivers. 


```python
conf_file = "C:\\windows\system32\reg.exe"
print(ntpath.splitdrive(conf_file))
```

    ('C:', '\\windows\\system32\reg.exe')


    <>:1: SyntaxWarning: invalid escape sequence '\s'
    <>:1: SyntaxWarning: invalid escape sequence '\s'
    /tmp/ipykernel_10982/2708325063.py:1: SyntaxWarning: invalid escape sequence '\s'
      conf_file = "C:\\windows\system32\reg.exe"



```python
import os.path
import glob

# Shows a list of file names
# and their respective sizes 
for arq in sorted(glob.glob('*.*')):
    print (f"{arq=}, {os.path.getsize(arq)=}")
```

    arq='01. Files and IO.ipynb', os.path.getsize(arq)=34150
    arq='01. Files and IO.md', os.path.getsize(arq)=17067
    arq='dummy.txt', os.path.getsize(arq)=0
    arq='eggs.csv', os.path.getsize(arq)=81
    arq='list.csv', os.path.getsize(arq)=33
    arq='march18_myspeed.csv', os.path.getsize(arq)=48115174
    arq='new_zip.zip', os.path.getsize(arq)=625
    arq='temp.txt', os.path.getsize(arq)=25
    arq='temp1.txt', os.path.getsize(arq)=28
    arq='temp2.txt', os.path.getsize(arq)=25
    arq='temp3.txt', os.path.getsize(arq)=25
    arq='temp4.txt', os.path.getsize(arq)=15


The *glob.glob()* function returns a list of filenames that meet the criteria passed as a parameter in a similar way to the `ls` command available on UNIX systems.

Temporary files
--------------------
The module *os* implements some functions to facilitate the creation of temporary files, freeing the developer from some concerns, such as:

+ Avoiding collisions with names of files that are in use.
+ Identifying the appropriate area of the file system for temporary files (which varies by operating system).
+ Not exposing the implementation risks (temporary area is used by other processes).

Example:


```python
import tempfile

# create a temporary file and write some data to it
fp = tempfile.TemporaryFile()
fp.write(b'Hello world!')
# read data from file
fp.seek(0)
fp.read()
```




    b'Hello world!'




```python
print(fp.name)
```

    56



```python
# close the file, it will be removed, but if you forget to 
# Close then python might not remove the temp file. 
# In *nix, the file is not created at all.

fp.close()
```


```python
# create a temporary file using a context manager
with tempfile.TemporaryFile() as fp:
    print(fp.name)
    fp.write(b'Hello world!')
    fp.seek(0)
    print(fp.read())

# file is now closed and removed
```

    56
    b'Hello world!'



```python
# create a temporary file using a context manager
with tempfile.TemporaryFile() as fp:
    print(fp.name)
    fp.write(b'Hello world!')
#     fp.seek(0)
    print(fp.read())

# file is now closed and removed
```

    56
    b''



```python
# create a temporary directory using the context manager
with tempfile.TemporaryDirectory() as tmpdirname:
    print('created temporary directory', tmpdirname)
    file_name = os.path.join(tmpdirname, "myfile.txt")
    print(file_name)
    with open(file_name, "w") as fp:
        fp.write("Hello world")
    from time import sleep
    sleep(20)
    print("timeout, delete the temp folder along with files")
    
# directory and contents have been removed
```

    created temporary directory /tmp/tmpva8enumu
    /tmp/tmpva8enumu/myfile.txt
    timeout, delete the temp folder along with files



```python
from time import sleep
    
try:
    with tempfile.TemporaryDirectory() as tmpdirname:
        print('created temporary directory', tmpdirname)
        file_name = os.path.join(tmpdirname, "myfile.txt")
        print(file_name)
        with open(file_name, "w") as fp:
            fp.write("Hello world")
        sleep(20)
        raise Exception("dummy Exception")
        print("This should never ever get executed.")
        sleep(20)
        print("timeout ")
except Exception as e:
    print("Error:", e)
```

    created temporary directory /tmp/tmpbbpikx3v
    /tmp/tmpbbpikx3v/myfile.txt
    Error: dummy Exception


The objects of type file also have the method `seek()`, which allow going to any position in the file.

## Compressed files

Python has modules to work with multiple formats of compressed files.

Example of writing a ".zip" file:


```python
"""
Writing text in a compressed file
There is always a better way and this it is not.
"""

import zipfile

text = """
**************************************
This text will be compressed and ...
... stored inside a zip file.
***************************************
"""

# Creates a new zip
zip = zipfile.ZipFile('new_zip.zip', 'w',
    zipfile.ZIP_DEFLATED)

# Writes a string in zip as if it were a file
zip.writestr('text.txt', text)
zip.writestr('text1.txt', text)
zip.writestr('text2.txt', text)
# closes the zip
zip.close()
```


```python
"""
Writing text in a compressed file
"""

import zipfile

text = """
**************************************
This text will be compressed and ...
... stored inside a zip file using 
... Context Manager on Zipfile module .
***************************************
"""

# Writes a string in zip as if it were a file
with zipfile.ZipFile('new_zip.zip', 'w', 
                     zipfile.ZIP_DEFLATED) as zipfile:
    zipfile.writestr('text11.txt', text)
    zipfile.writestr('text12.txt', text)
    zipfile.writestr('text23.txt', text)
```

**Reading example:**


```python
"""
Reading a compressed file
"""
import zipfile

# Open the zip file for reading 
with zipfile.ZipFile('new_zip.zip') as zip:
    # Gets a list of compressed files
    file_list = zip.namelist()
    print("Files: ", file_list)
    for arq in file_list:
        # Shows the file name
        print(f'File: {arq}')
        # get file info
        zipinfo = zip.getinfo(arq)
        print('Original size:', zipinfo.file_size)
        print('Compressed size:', zipinfo.compress_size)
    #     print(dir(zipinfo))

        # Shows file content
        print(zip.read(arq))
```

    Files:  ['text11.txt', 'text12.txt', 'text23.txt']
    File: text11.txt
    Original size: 193
    Compressed size: 105
    b'\n**************************************\nThis text will be compressed and ...\n... stored inside a zip file using \n... Context Manager on Zipfile module .\n***************************************\n'
    File: text12.txt
    Original size: 193
    Compressed size: 105
    b'\n**************************************\nThis text will be compressed and ...\n... stored inside a zip file using \n... Context Manager on Zipfile module .\n***************************************\n'
    File: text23.txt
    Original size: 193
    Compressed size: 105
    b'\n**************************************\nThis text will be compressed and ...\n... stored inside a zip file using \n... Context Manager on Zipfile module .\n***************************************\n'


Python also provides modules for gzip, bzip2 and tar formats that are widely used in UNIX environments.

Data file
----------------
In the standard library, Python also provides a module to simplify the processing of files in CSV (*Comma Separated Values*) format.

In CSV format, the data is stored in text form, separated by commas, one record per line.

Writing example:


```python
import csv

with open('eggs.csv', 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(['Spam'] * 5 + ['Baked Beans'])
    csv_writer.writerow(['Spam', None ,'Lovely "Spam"', 'Wonderful, Spam'])
```


```python
import csv

with open('eggs.csv', 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile)
    csv_writer.writerow(['Spam'] * 5 + ['Baked Beans'])
    csv_writer.writerow(['Spam', None ,'Lovely "Spam"', 'Wonderful, Spam'])
```


```python
lst = [["a", "a1"], ["b", "b1"], ["d", "d1 ,2,one"], "c", "f" ]
with open('list.csv', 'w', newline='') as csvfile:
    listWriter = csv.writer(csvfile)
    for l in lst:
        listWriter.writerow(l)
```

Reading example:


```python
import csv

with open("list.csv", newline='') as csvfile:
    for row in csv.reader(csvfile, delimiter=' ', quotechar='|'):
        print(',   '.join(row))
```

    a,a1
    b,b1
    d,"d1,   ,2,one"
    c
    f



```python
import csv

with open("eggs.csv", newline='') as csvfile:
    for row in csv.reader(csvfile, delimiter=' ', quotechar='|'):
        print(',   '.join(row))
```

    Spam,Spam,Spam,Spam,Spam,Baked,   Beans
    Spam,,"Lovely,   ""Spam""","Wonderful,,   Spam"


The CSV format is supported by most spreadsheet and databases for data import and export.

## IO

In Python:

+ *sys.stdin* is the standard input.
+ *sys.stdout* is the standard output.
+ *sys.stderr* is the standard error output.

The standard `input`, `output` and `error` are handled by Python as `open` files. 

Example of using std*'s:

### `sys.stdout`

#### `sys.stdout.write`


```python
from sys import stdout

stdout.write("Hello World")
stdout.write("Hello World")
stdout.write("Hello World")
```

    Hello WorldHello WorldHello World




    11



### `print`


```python
data = ["Welcome", "Ja", "Nein", "Kind"]

with open("ja.txt", "w") as fp:
    print(f"{data}", file=fp)
```


```python
data = ["Welcome", "Ja", "Nein", "Kind"]

with open("ja.txt", "w") as fp:
    for d in data:
        print(f"{d}", file=fp)
```


```python
data = ["Welcome", "Ja", "Nein", "Kind"]

with open("ja.txt", "w") as fp:
    print(data, file=fp)
```

##  Excersise - Files I/O

<img style="float:left;width:4em;height:4em;margin-left: unset;margin-right: 2em;margin-top: unset;" src="../../imgs/Quests.png"> 

1. Write a Python program to read an entire text file.     
2. Write a Python program to read first n lines of a file.     
3. Write a Python program to append text to a file and display the text.     
4. Write a Python program to read last n lines of a file.     
5. Write a Python program to read a file line by line and store it into a list.     
6. Write a Python program to read a file line by line store it into a variable.     
7. Write a Python program to read a file line by line store it into an array.     
8. Write a python program to find the longest words.     
9. Write a Python program to count the number of lines in a text file.     
10. Write a Python program to count the frequency of words in a file.     
11. Write a Python program to get the file size of a plain file.     
12. Write a Python program to write a list to a file.     
13. Write a Python program to copy the contents of a file to another file .     
14. Write a Python program to combine each line from first file with the corresponding line in second file.     
15. Write a Python program to read a random line from a file.     
16. Write a Python program to assess if a file is closed or not.     
17. Write a Python program to remove newline characters from a file.     
