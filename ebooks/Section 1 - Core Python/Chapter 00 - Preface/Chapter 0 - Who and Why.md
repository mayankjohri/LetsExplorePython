# Chapter 0 - Who & Why
---

This tutorial is created for the sole purpose for provide a base document for my training programs. I am releasing it on the net in hope of people find it useful. 



## Who 
---


I am Mayank Johri and since 2000 I have been working in IT domain. In this time I was able to release few opensource projects and books on it. 

Projects can be found at 

- https://sourceforge.net/projects/softgridhelper/
- https://gitlab.com/mayankjohri

and books at Amazon. 

> **NOTE:**
> This version has few topics as placeholder which will be completed in future version only such as 
>
> - Chapter 16 - Standard library,
> - Chapter 20 - Basic Design Patterns,
> - Chapter 21 - Type Hinting,
> - Chapter 99 - Debugging_todo
>
> Also, many newly added topics might look partially completed. 

### My history with Python

Till 2005, Freebasic, VB, VB.NET & C were my primary programming languages. In early 2005, due to certain personal & political (read M$) reasons, I started searching for my next primary language. My requirement for it were few as listed below:
- Opensource language (so I don't have to pay, no licensing issues, no change in language without some workaround for changes)
- Easy to learn (Never had too much time)
- can be compiled to executables
- GUI programming should be possible (wx)
- Relatively decent execution speed
- Decent support, documentation & forum

After about six months, and trying almost most the programming languages from (https://en.wikipedia.org/wiki/List_of_programming_languages)[https://en.wikipedia.org/wiki/List_of_programming_languages], I found that python language suite most of my requirements and thus it became my first choice of programming language.

Other langauges which came very near were: "Ruby", "FreeBasic", "C" & "lua". I liked "FreeBasic" and also migrated one crutial utility of `SoftGridHelper` to it from Python/.Net, since, that utility was to always running on user machines and was to be installed on nearly every machine on the client infrastructure (excluding servers) and "FreeBasic" provided me an exeuctable with in few "kb's" and without horrible dependency issues of `.NET` versions and it was used in few corporates without any issue.


```python
# This documented has been created using jupyter
## Python version used: 
import sys
print("Python Version Used:", sys.version)
print(sys.version_info)
```

    Python Version Used: 3.10.7 (main, Mar 10 2023, 10:47:39) [GCC 12.2.0]
    sys.version_info(major=3, minor=10, micro=7, releaselevel='final', serial=0)


## Icons and their meanings

| Icon | Meaning | 
|:------:|:--------|
| <img style="float:left;width:4em;height:4em;margin-left: unset;margin-right: 1em;margin-top: unset;" src="../../imgs/tips-new1.png"> | Tips which you can use |
| <img style="float:left;width:4em;height:4em;margin-left: unset;margin-right: 1em;margin-top: unset;" src="../../imgs/tripping.png">  | Tripping/Gotcha's you should avoid | 
| <img style="float:left;width:4em;height:4em;margin-left: unset;margin-right: 1em;margin-top: unset;" src="../../imgs/notes-new1.png">   | Notes: key points we should remember | 
| <img style="float:left;width:4em;height:4em;margin-left: unset;margin-right: 1em;margin-top: unset;" src="../../imgs/Quests.png">   | Notes: key points we should remember | 
