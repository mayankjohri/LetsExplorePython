# Chapter 3:Traditional Control Flow Constructs

Compound statements contain one or groups of other statements; they affect or control the execution of those other statements in some way. 

In general, they span multiple lines, but can be also be listed in a single line.

The `if`, `while`, `for` and `match/case` statements implement **traditional control flow constructs**, whereas `try` specifies **exception handlers and/or cleanup code** for a group of statements, while the `with` statement allows the execution of initialization and finalization code around a block of code. `Function` and `class` definitions are also **syntactically compound statements**.

They consists of one or more *‘clauses’*. A *clause* consists of a 'header' and a ‘suite’. 

The 'clause' headers of a particular compound statement are all at the same indentation level. They should begins with an uniquely identifying keyword and should ends with a colon. 

'suite' is a group of statements controlled by a clause. It can be of one or more semicolon-separated simple statements on the same line as the header, following the header’s colon (one liner), or it can be one or more indented statements on subsequent lines. Only the latter form of a suite can contain nested compound statements; the following is illegal, mostly because it wouldn’t be clear to which if clause a following else clause would belong:

With Python 3.10, `Structural Pattern Matching` has been added to the control flow, which also we will discuss in this chapter

## `if` Statement

The `if` statement is used for conditional execution similar to that in most common languages. If statement can be constructed in three format depending on our need.

- **if**:  when we have "**if** something do something" condition
- **if .. else**: when we have condition like "**if** something: do something **else** do something else" 
- **if .. elif ..else**: When we have too many conditions or nested conditions



### `if`


This format is used when specific operation needs to be performed if a specific condition is met. 
Syntax:
```python
if <condition>:
    <code block>
```


Where:

+ `<condition>`: sentence that can be evaluated as true or false.
+ `<code block>`: sequence of command lines.
+ The clauses `elif` and `else` are optional and  several `elifs` for the `if` may be used but only  one `else` at the end.
+ Parentheses are only required to avoid ambiguity.
Example:


```python
x = "Vinay"
y = "Vinod"

if y == "Vinod":
    print(x)
```

    Vinay



```python
if y:
    print("Welcommone", y)
```

    Welcommone Vinod



```python
# Falsish Value, thus will not run the if code block
y = None

if y:
    print("TEST")
```


```python
y = 0

if y:
    print("TEST")
```


```python
y = []

if y:
    print("TEST")
```


```python
# As the list is not empty, the `y` is holding 
# Truish value. 

y = [False]

if y:
    print("TEST")
```

    TEST



```python
y = [[]]

if y:
    print("TEST")
```

    TEST



```python
y = {}

if y:
    print("TEST")
```


```python
y = {2}

if y:
    print("TEST")
```

    TEST



```python
y = {2}

if y:
    print("TEST 1 ")
    print("TEST 2")
    print("TEST 3")
```

    TEST 1 
    TEST 2
    TEST 3



```python
y = set()

if y:
    print("TEST")
```


```python
# For C, C# & Java Guys
passcode = "Simsim"
if passcode == "Simsim": # {
    print("\t> Welcome to the cave")
# }

print("Bye bye")
```

    	> Welcome to the cave
    Bye bye



```python
# For C, C# & Java Guys

passcode = "SimSim"
if passcode == "Simsim":  # {
    print("\t> Welcome to the cave")
# }

print("Bye bye")
```

    Bye bye


#### Cascading of `if`

Cascading of `if` statements is possible as shown in below example.


```python
x = 12
if x > 10:
    print("Hello")
    if x > 10.999999999999:
        print("Hello again")
        if x % 2 == 0:   # Checking if number is even
            print("Bye bye bye ...")
```

    Hello
    Hello again
    Bye bye bye ...



```python
x = 11
if x > 10:
    print("Hello")
    if x > 10.999999999999:
        print("Hello again")
        if x % 2 == 0:
            print("Bye bye bye ...")
```

    Hello
    Hello again



```python
x = 10.2

if x > 10:
    print("Hello")
    if x > 10.999999999999:
        print("Hello again")
        if x % 2 == 0:
            print("Bye bye bye ...")
```

    Hello



```python
x = "Green"
y = None
z = "111"
print(id(y))

if x == "Green":
    print("Hello x is Green")

if x == 'Yellow':
    print("Hello in Y")

if x == 'Orange':
    print("Hello in Z")
```

    139781001690272
    Hello x is Green


### `if` ... `else` statement

```python
if <condition>:
    <code block One>
else:
    <code block Second>
```
Where:

+ `<condition>`: sentence that can be evaluated as true `if` statement `if` statemente or false.
+ `<code block>`: sequence of command lines.
+ The clauses `elif` and `else` are optional and  several `elifs` for the `if` may be used but only  one `else` at the end.
+ Parentheses are only required to avoid ambiguity.


```python
user = "Anuja"

if user == "mayank":  # {
    print("Name is mayank")
# }
else:  # {
    print(f"Name is not mayank and its {user}")
# }
```

    Name is not mayank and its Anuja



```python
user = "Anuja"

if user != "mayank":
    print(f"Name is not mayank and its {user}")
else:
    print("Name is mayank")
```

    Name is not mayank and its Anuja



```python
x = "Orange"

if x == "Green":
    print("Hello x is Green")
else:
    if x == 'Yellow':
        print("Hello in Yellow")
    else:
        if x == 'Orange':
            print("Hello in Orange")
        else:
            print("Sorry, required color not found")
```

    Hello in Orange



```python
x = "Blue"

if x == "Green":
    print("Hello x is Green")
else:
    if x == 'Yellow':
        print("Hello in Yellow")
    else:
        if x == 'Orange':
            print("Hello in Orange")
        else:
            print("Sorry, required color not found")
```

    Sorry, required color not found


### `if` ...`elif` ... `else` statement

**Syntax:**

```python
if <condition>:
    <code block>
elif <condition>:
    <code block>
elif <condition>:
    <code block>
else:
    <code block>
```

Where:

+ `<condition>`: sentence that can be evaluated as true or false.
+ `<code block>`: sequence of command lines.
+ The clauses `elif` and `else` are optional and  several `elifs` for the `if` may be used but only  one `else` at the end.
+ Parentheses are only required to avoid ambiguity.


```python
# In this case we will exit after executing 
# first true condition block
cols = "Orange", "Red", "Green"

orange, red, green = "Orange", "Red", "Green"

if orange in cols:
    print("Great, it contains ", orange)
elif red in cols:
    print("Great, it contains ", red)
elif green in cols:
    print("Great, it contains ", green)
```

    Great, it contains  Orange



```python
# temperature value used to test
# Logical Issue ? Find it if you can.

temp = 20

if temp < 0:
    print('Freezing...')
elif 0 <= temp <= 20:
    print ('Cold')
elif 21 <= temp <= 25:
    print('Room Temprature')
elif 26 <= temp <= 35:
    print ('Hot')
else:
    print('Its very HOT!, lets stay at home... \nand drink lemonade.')
```

    Cold



```python
# temperature value used to test
# Logical Issue ? Find it if you can.

temp = 45

if temp < 0:
    print('Freezing...')
elif 0 <= temp <= 20:
    print ('Cold')
elif 21 <= temp <= 25:
    print('Room Temprature')
elif 26 <= temp <= 35:
    print ('Hot')
else:
    print('Its very HOT!, lets stay at home... \nand drink lemonade.')
```

    Its very HOT!, lets stay at home... 
    and drink lemonade.



```python
# temperature value used to test
temp = 25.3

if temp < 0:
    print('Freezing...')
elif 0 <= temp <= 20:
    print ('Cold')
elif 21 <= temp <= 25:
    print('Room Temprature')
elif 26 <= temp <= 35:
    print ('Hot')
else:
    print('Its very HOT!, lets stay at home... \nand drink lemonade.')
```

    Its very HOT!, lets stay at home... 
    and drink lemonade.



```python
# temperature value used to test
# logical issue solved. 

temp = 25.2

if temp <= 0:
    print('Freezing...')
elif 0 < temp <= 20:
    print ('Cold')
elif 20 < temp <= 25:
    print('Room Temprature')
elif 25 < temp <= 35:
    print ('Hot')
else:
    print('Its very HOT!, lets stay at home... \nand drink lemonade.')
```

    Hot


Imagine that in the above program, `23` is the temperature which was read by  some sensor or manually entered by the user and `Normal` is the response of the program.


```python
# temperature value used to test
# Much simpler code 

temp = 12

if temp <= 0:
    print('Freezing...')
elif temp <= 20:
    print ('Cold')
elif temp <= 25:
    print('Room Temprature')
elif temp <= 35:
    print ('Hot')
else:
    print('Its very HOT!, lets stay at home... \nand drink lemonade.')
```

    Cold


> <img style="float:left;width:4em;height:4em;margin-left: unset;margin-right: 1em;margin-top: unset;" src="../../imgs/notes-new1.png">  
> 
> This construct will only run the first true `if` block and will skip all the others as shown in the below example


```python
a = "apple"
b = "banana"
c = "Mango"

if a == "apple":
    print("a is apple")
elif b == "banana":
    print("b is banana")
elif c == "Mango":
    print("My Mango farm")
```

    a is apple



```python
x = "Orange"

if x == "Green":
    print("Hello x is Green")
elif x == 'Yellow':
    print("Hello in Y")
elif x == 'Orange':
    print("Hello in Z")
```

    Hello in Z



```python
user_selection = "Orange"
orange, red, green = "Orange", "Red", "Green"

if user_selection == orange:
    print("Hello user_selection is", orange)
elif user_selection == red:
    print("Hello user_selection is", red)
elif user_selection == green:
    print("Hello user_selection is", green)
```

    Hello user_selection is Orange


### One line if 

There are instances where we might need to have 
If the code block is composed of only one line, it can be written after the colon:

    if temp < 0: print 'Freezing...'


```python
x = 20

if x > 10: print("Hello ")
```

    Hello 



```python
# Intresting example
x = 20

if x > 10: print("Hello "); print("How are you doing")
```

    Hello 
    How are you doing



```python
# Intresting example
x = 3

if x > 10: print("Hello "); print("How are you doing")
```

Since version 2.5, Python supports the expression:
```python
<variable> = <value 1> if <condition> else <value 2>
```
Where `<variable>` receives `<value 1>` if `<condition>` is true and `<value 2>`  otherwise.


```python
x = 3

val = 1 if x < 10 else 24

print(f"{val = }, {x = }, {(x < 10) = }")
```

    val = 1, x = 3, (x < 10) = True



```python
val = 1 if x > 10 else 24

print(f"{val = }, {x = }, {(x < 10) = }")
```

    val = 24, x = 3, (x < 10) = True



```python
# Cascaded if eles statements in a single line

val = (1 if x > 10 else 24) if x % 2 == 0 else 100

print("val:", val, "x:", x, "x > 10:", x > 10)
```

    val: 100 x: 3 x > 10: False



```python
val = 1 if x > 10 else 24 if x % 2 != 0 else 100

print("val:", val, "x:", x, "x > 10:", x > 10)
```

    val: 24 x: 3 x > 10: False



```python
val = 1 if x > 10 else 24 if x % 2 == 0 else 100

print("val:", val, "x:", x, "x > 10:", x > 10)
```

    val: 100 x: 3 x > 10: False


### `if` expression extended

we can extend one liner `if` using `()`'s as shown in the below example. 


```python
# Never ever use this code in Production

temp = 30

(print ('Freezing...') if temp < 0 else
print ('Cold') if 0 < temp <= 20 else
print ('Room Temprature') if 20 < temp <= 25 else 
print("Hot") if 25 < temp <= 35 else
print ('Its very HOT!, lets stay at home... \nand drink lemonade.'))
```

    Hot



```python
# Optimized code
# But still Never ever use this code in Production
temp = 30

print(('Freezing...' if temp < 0 else
       'Cold' if 0 < temp <= 20 else
       'Room Temprature' if 20 < temp <= 25 else 
       "Hot" if 25 < temp <= 35 else
       'Its very HOT!, lets stay at home... \nand drink lemonade.'))
```

    Hot


We have converted one previous multi `if-elif-else` example to `single line if-else` construct.

### Suite

Python allows to have more than one statements on a single line using `;` and its called `suite` (https://docs.python.org/3/reference/compound_stmts.html). Very simple suite is as shown in the exmples below


```python
print("He", end=""); print("l", end=""); print("lo")
```

    Hello



```python
x = 10; print(x); x += 20; print(x)
```

    10
    30


as you can see in the above example we can have more than one statements in a simple line. Lets check its effect on other compound constructs

### `Suite` and `if`

The entire suite will get executed only when the `if` condition is fulfilled.


```python
x = 10
if x <= 10: print("He", end=""); print("l", end=""); print("lo")
print("...")
```

    Hello
    ...


else the entire suite will not get executed.


```python
x = 30
if x <= 10: print("He", end=""); print("l", end=""); print("lo")
print("...")
```

    ...


> <img style="float:left;width:4em;height:4em;margin-left: unset;margin-right: 1em;margin-top: unset;" src="../../imgs/notes-new1.png">  
>
> The semicolon binds tighter than the colon in the above context, thus either all or none of the `print()` calls are executed

#### Gotcha's

1. We can only have first statement as compound statements and all the following cannot be compound statementas (statements with `:`) as shown in the examples below.

```python
x = 20; if x <= 10: print("l", end=""); print("lo")
```
**Output:**
```python
  File "<ipython-input-14-cf119c4c1398>", line 2
    x = 20; if x <= 10: print("l", end=""); print("lo")
             ^
SyntaxError: invalid syntax
``` 

As one liner `if` do not have `:` in its construct it can be present in following statements as shown in the below example


```python
size = 10
color = "blue"

if size > 8: print("> 8 and blue in color") if color == "blue" else print("> 8 but not blue color");print("Swagatam")
```

    > 8 and blue in color
    Swagatam


The below example will not work as last statement was ":" in it, 
```python
# this one will not work
x = 20
if x > 10: print("Greater than 10") if x > 15 else print("less than") else: print("TEST")
```
**Output:**
```python
  File "<ipython-input-34-ad260d588f22>", line 2
    if x > 10: print("Greater than 10") if x > 15 else print("less than") else: print("TEST")
                                                                             ^
SyntaxError: invalid syntax
```

But, following will work without issue. 


```python
size = 10
color = "blue"
if size > 8: x = 10 and True or False; print(x) 
# (x = 10 and True) or False
# True -> 10 and True
# True -> True or False
# True
```

    True



```python
if size > 8: print("> 8 and blue in color"); print(10) if color == "blue" else print("> 8 but not blue color"); print("Hello ")
```

    > 8 and blue in color
    10
    Hello 


### Uses of suite

#### running at command prompt/interpretor

```python
$:> python -c "import os; print(os.path.join('test', 'test'))"
```

#### initiating pdb

easy to write and easy to remove. 


```python
# import pdb; pdb.sta...
```

#### Ending multiline statement


```python
temp = 30

print(('Freezing...' if temp < 0 else
'Cold' if 0 < temp <= 20 else
'Room Temprature' if 20 < temp <= 25 else 
"Hot" if 25 < temp <= 35 else
'Its very HOT!, lets stay at home... \nand drink lemonade.'));
```

    Hot


#### for c/c++ guys (joke)


```python
# now your code looks like c/c++ :)

temp = 20;
print(temp);
```

    20


### Assignment expression Aka Walrus Operator  `:=` and `if` statement

`walrus` operator allows developers to assign variables within an expression, which then can be later used without the need to recalculate it when later needed in the block.

There are multiple usecases for walrus operator in `if` statements. Few of the examples below provide the usescase described in the https://www.python.org/dev/peps/pep-0572/ (PEP 572 -- Assignment Expressions).

> Note: `walrus` operator has been added in Python 3.8. 


```python
# Normal Equivalent code without using walrus operator.

txt = "Its very HOT!, lets stay at home..."

if len(txt) > 10:
    width = len(txt)
    print(f"Length of str: {txt} is {width}")
```

    Length of str: Its very HOT!, lets stay at home... is 35



```python
# Normal Optimization Equivalent code without using walrus operator.

txt = "Its very HOT!, lets stay at home... \nand drink lemonade."

txt_len = len(txt)

if txt_len > 10:
    width = txt_len
    print(f"Length of str: {txt} is {width}")
```

    Length of str: Its very HOT!, lets stay at home... 
    and drink lemonade. is 56



```python
# Optimized using walrus operator.

txt = "Its very HOT!, lets stay at home... \nand drink lemonade."

if (width := len(txt)) > 10:
    print(f"Length of str: {txt} is {width}")
```

    Length of str: Its very HOT!, lets stay at home... 
    and drink lemonade. is 56


In the above example, variable `width` is assigned in the `if` statemenet itself and later used within the `if` code block. Prior to `3.8`. 


```python
txt = "Krivanto Visvamaryam"

if (num := txt.count("m")) > 1:
    print(f"`m` found {num} times in text: {txt}")
```

    `m` found 2 times in text: Krivanto Visvamaryam


In the above examples, we have seen a usecase where we need to get the value from the `if` condition. In those cases we can use **walrus operator `:=` which assigns values to variables as part of a larger expression as shown in the below examples


```python
def get_val(name):
    return name.lower()

if (name := get_val("Mayank")) == "mayank":
    print(f"Welcome {name}")
```

    Welcome mayank



```python
if (name := get_val("Mohan")) == "mayank":
    print(f"Welcome {name}")
else:
    print(f"Good Bye {name}")
```

    Good Bye mohan



```python
def total(*a):
    result = 0
    for x in a:
        result += x
    return result

if (final_count := total(10, -7, -3)):
    print(f"Remaining Persons in Zoo: {final_count = }")
else:
    print(f"All have left the zoo as final count is: {final_count = }")
```

    All have left the zoo as final count is: final_count = 0



```python
if (final_count := total(10, -5, -3)):
    print(f"Remaining Persons in Zoo: {final_count = }")
else:
    print(f"All have left the zoo as final count is: {final_count = }")
```

    Remaining Persons in Zoo: final_count = 2


## Structural Pattern Matching

(Structural Pattern Matching) `match` and `case` keywords are **soft keywords** and used with following syntax

```python

match subject:
    case <pattern_1>:
        <action_1>
    case <pattern_2>:
        <action_2>
    case _:
        <action_wildcard>
```        

**Example:**


```python
def abbreviation_meaning(abbreviation):
    match abbreviation:
        case "Dr":
            return "Doctor"
        case "Mr":
            return "Mister"
        case "Mrs":
            return "Missus"
        case _:
            return "Not Sure"
        
print(f'{abbreviation_meaning("Mr") = }')
```

    abbreviation_meaning("Mr") = 'Mister'



```python
print(f'{abbreviation_meaning("Jr") = }')
```

    abbreviation_meaning("Jr") = 'Not Sure'


In the above example, the `match` construct will sequentially check the `abbreviation`against the `case` values and will run the code block of the first matched case. If not able to find any matches then will run the code block under case `_`, if provided.

Lets learn about the options provided by it.

### Simple pattern: match to a literal

Lets take a look at previous example again, in that example, we have only one value in `case` each. It's the simplest usecase which can be created. 

If I provide `Dr` value for `abbreviation` then it will return `Doctor` and if `Mrs` is provided then `Missus` is returned.

We can even provide multiple values in `case` by using `|` (or) as shown in below example.


```python
def abbreviation_meaning(abbreviation):
    match abbreviation:
        case "Raja" | "King":
            return "kingdom"
        case "President" | "Prime Minister":
            return "Democracy"
        case "Dictator" | "czar":
            return "dictatorship"
        case _:
            return "Not Sure"
        
```


```python

print(f'{abbreviation_meaning("Raja") = }')
```

    abbreviation_meaning("Raja") = 'kingdom'



```python

print(f'{abbreviation_meaning("czar") = }')
```

    abbreviation_meaning("czar") = 'dictatorship'



```python

print(f'{abbreviation_meaning("BDFL") = }')
```

    abbreviation_meaning("BDFL") = 'Not Sure'


In the below example, we have removed the default `case _`. Now if we request with a non existing case value, then no case is selected.


```python
abbreviation = "Testing"

match abbreviation:
    case "Raja" | "King":
        print("kingdom")
    case "President" | "Prime Minister":
        print("Democracy")
    case "Dictator" | "czar":
        print("dictatorship")
        
print("Done")
```

    Done


### `match` and unpacking

Pattern variables can also be used for unpacking data as shown in below example   


```python
validate = (True, "Correct")

match validate:
    case (True, "Wrong"):
        print("True Wrong Message")
    case (True, "Correct"):
        print("Correct Message")
    case (False, "Wrong"):
        print("Wrong Message")
    
```

    Correct Message


There are times when we only need to check part of the pattern as shown in two below examples


```python
validate = (True, "Correct")

match validate:
    case (True, msg):
        print(f"Good Message Received: {msg}")
    case (False, msg):
        print(f"Bad Message Received: {msg}")
```

    Good Message Received: Correct



```python
validate = (False, "Something is wrong")

match validate:

    case (True, msg):
        print(f"Good Message Received: {msg}")
    case (False, msg):
        print(f"Bad Message Received: {msg}")
```

    Bad Message Received: Something is wrong


We can sometime have need to have full match or partial match of pattern as show in two below examples.


```python
validate = (True, "Correct")

match validate:
    case (True, "Correct"):
        print("Default True message received")
    case (True, msg):
        print(f"Good Message Received: {msg}")
    case (False, msg):
        print(f"Bad Message Received: {msg}")
```

    Default True message received



```python
validate = (True, "Yada Yada")

match validate:
    case (True, "Correct"):
        print("1. Default True message received")
    case (True, msg):
        print(f"2. Good Message Received: {msg}")
    case (False, msg):
        print(f"3. Bad Message Received: {msg}")
```

    2. Good Message Received: Yada Yada



```python
# Only the first full or partial case match code block 
# will be executed.

validate = (True, "Correct")

match validate:
    case (True, msg):
        print(f"Good Message Received: {msg}")
        
    case (True, "Correct"):
        print("Default True message received")
    
    case (False, msg):
        print(f"Bad Message Received: {msg}")
```

    Good Message Received: Correct


### Nested patterns

We can even match nested patterns as shown in the below example


```python
lst = ["Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
    case ["Mayank"]:
        print("Mayank Present")
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

    Mayank Johri Present


Now, lets check the below example and compare if with the result of previous example


```python
lst = ["Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
    case ["Mayank", last_name]:
        print(f"Mayank with {last_name = } Present")
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

    Mayank with last_name = 'Johri' Present


### Complex patterns and the wildcard

There are times when you just want to check a particular condition and ignore the remaining data. We can achive it by using `_` as shown in the below example.

Please note that `_` can be used to ignore only one element as shown in the last example in the section. 


```python
lst = ["Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
    
    case ["Mayank", _]:
        print(f"Mayank with {_ = } Present")
        
    case ["Mayank", last_name]:
        print(f"Mayank with {last_name = } Present")
    
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

    Mayank with _ = '' Present



```python
lst = ["Mayank", "Johri"]

match lst:
    case []:
        print("1. Nothing present")
    case [_, "Johri", _]:
        print(f"2. Mayank with { _ = } Present")
    case ["Mayank", last_name]:
        print(f"3. Mayank with {last_name = } Present")
    case ["Mayank", "Johri"]:
        print("4. Mayank Johri Present")
```

    3. Mayank with last_name = 'Johri' Present



```python
lst = ["Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
    case [_, "Johri"]:
        print(f"Mayank with {_ = } Present")
    case ["Mayank", last_name]:
        print(f"Mayank with {last_name = } Present")
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

    Mayank with _ = '' Present



```python
lst = ["Shri", "Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
        
    case [_, "Johri", _]:
        print(f"Mayank with {_ = } Present")
        
    case ["Mayank", last_name]:
        print(f"Mayank with {last_name = } Present")
    
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```


```python
lst = ["Shri", "Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
        
    case [_, _, "Johri"]:
        print(f"_, _, Johri")
    
    case ["Mayank", last_name]:
        print(f"Mayank with {last_name = } Present")
    
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

    _, _, Johri


### Guard

We can even add `if` conditions in our case as shown below


```python
lst = ["Shri", "Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
    
    case [_, _, "Johri"] if len(lst) == 1:
        print("_, _, Johri with len == 1")
    
    case [_, _, "Johri"] if len(lst) == 3:
        print(f"_, _, Johri with len == 3 ")
    
    case ["Mayank", last_name]:
        print(f"Mayank with {last_name = } Present")
    
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

    _, _, Johri with len == 3 


### wildcard patterns 


```python
lst = ["Shri", "Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
    case [*dummy, "Johri"]:
        print(f"case [*dummy, Johri] with ignored value {dummy = }")
    case ["Mayank", last_name]:
        print(f"Mayank with {last_name = } Present")
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

    case [*dummy, Johri] with ignored value dummy = ['Shri', 'Mayank']


Please note that we **can not** use multiple wildcards in the pattern as shown in below example

```python
lst = ["Shri", "Mayank", "Johri"]

match lst:
    case []:
        print("Nothing present")
    case [*s, "Mayank", *last_name]:
        print(f"Mayank with {last_name = } Present")
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

**Output:**
```python
  File "<ipython-input-81-b8f4053d8cd5>", line 6
    case [*s, "Mayank", *last_name]:
         ^
SyntaxError: multiple starred names in sequence pattern
```

`match` also support complex data type pattern matching as shown in the below example


```python
lst = ["Shri", ("Mayank", "Johri")]

match lst:
    case []:
        print("Nothing present")
        
    case [*s, ("Mayank", *last_name)]:
        print(f"Mayank with {last_name = } and { s = }")
    
    case ["Mayank", "Johri"]:
        print("Mayank Johri Present")
```

    Mayank with last_name = ['Johri'] and  s = ['Shri']



```python
resp = {"code": "200", "data": {"name": "Mayank", "surname": "Johri"}}

match resp.get("code"):
    case "200":
        print(f"Got {resp = }")
```

    Got resp = {'code': '200', 'data': {'name': 'Mayank', 'surname': 'Johri'}}


## References

- https://docs.python.org/3/reference/compound_stmts.html
- https://docs.python.org/release/3.10.4/whatsnew/3.10.html.0
...
