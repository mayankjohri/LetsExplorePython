## Binary Data Services

### `struct` - Interpret bytes as packed binary data

Python provides `struct` module to convert native python data types into string of byes and vice versa. It can be used to communicate between `C` structs and `Python` data types and is usually used to handle binary data files. 

We need to import the `struct` module before using it.


```python
import struct
```

#### `struct.pack`

`struct.pack` can be used to pack the hashable datasets into binary data as shown in the below examples


```python
data = struct.pack('s i s', b'mayank', 45, b'Author')
print(f"{data = }")
```

    data = b'm\x00\x00\x00-\x00\x00\x00A'


In the above example, first string which is provided informs us about the data types of the remaining data. We can use the following format table to define the data types

| Format | C Type               | Python type       | Standard size |
| ------ | -------------------- | ----------------- | ------------- |
| `x`    | `pad byte`           | no value          |               |
| `c`    | `char`               | bytes of length 1 | 1             |
| `b`    | `signed char`        | integer           | 1             |
| `B`    | `unsigned char`      | integer           | 1             |
| `?`    | `_Bool`              | bool              | 1             |
| `h`    | `short`              | integer           | 2             |
| `H`    | `unsigned short`     | integer           | 2             |
| `i`    | `int`                | integer           | 4             |
| `I`    | `unsigned int`       | integer           | 4             |
| `l`    | `long`               | integer           | 4             |
| `L`    | `unsigned long`      | integer           | 4             |
| `q`    | `long long`          | integer           | 8             |
| `Q`    | `unsigned long long` | integer           | 8             |
| `n`    | `ssize_t`            | integer           |               |
| `N`    | `size_t`             | integer           |               |
| `e`    | -6                   | float             | 2             |
| `f`    | `float`              | float             | 4             |
| `d`    | `double`             | float             | 8             |
| `s`    | `char[]`             | bytes             |               |
| `p`    | `char[]`             | bytes             |               |
| `P`    | `void *`             | integer           |               |






```python
data = struct.pack('s i s', b'mayank', 45, b'Author')
print(f"{data = }")
```

    data = b'm\x00\x00\x00-\x00\x00\x00A'



```python

data = struct.pack('sis', b'mayank', 45, b'Author')
print(f"{data = }")
```

    data = b'm\x00\x00\x00-\x00\x00\x00A'


Providing other data type will result in exception as shown in below examples.


```python
try:
    data = struct.pack('sis', b'mayank', 1111111111111111111111111145, b'Author')
    print(f"{data = }")
except Exception as e:
    print(f"Error: {e}")
```

    Error: argument out of range



```python
try:
    data = struct.pack('sis', b'mayank', 123, 'Author')
    print(f"{data = }")
except Exception as e:
    print(f"Error: {e}")
```

    Error: argument for 's' must be a bytes object


#### `struct.unpack`

`struct.pack` can be used to unpack the hashable datasets from the binary data as shown in the below examples. Also note that it always return a tuple with unpacked data in it.


```python
py_data = struct.unpack("s i s", data)
print(f"{py_data = }")
```

    py_data = (b'm', 45, b'A')


#### `struct.calcsize()`

when the string representation of struct is provided to `struct.calcsize()`, returns its calculated size as shown in the below examples


```python
str_size = struct.calcsize("s i s")
print(f"{str_size = }")
```

    str_size = 9



```python
str_size = struct.calcsize("4f i s")
print(f"{str_size = }")
```

    str_size = 21



```python
str_size = struct.calcsize("4f d")
print(f"{str_size = }")
```

    str_size = 24


#### `struct.iter_unpack(format, buffer)`

Iteratively unpack from the `buffer` buffer according to the `format` string format. It returns an iterator which will read equally-sized chunks from the `buffer` until all its contents have been consumed. The buffer’s size in bytes must be a multiple of the size required by the format, as reflected by `calcsize()`.

Each iteration yields a tuple as specified by the format string.


```python
data = struct.pack("4s i 4s i", b"maya", 10, b"john", 30)

for x in struct.iter_unpack("4s i",data):
    print(f"{x}")
```

    (b'maya', 10)
    (b'john', 30)



```python
data = struct.pack("i i i i", 10, 20, 30, 40)

for x in struct.iter_unpack("i",data):
    print(f"{x = }")
```

    x = (10,)
    x = (20,)
    x = (30,)
    x = (40,)



```python
data = struct.pack("2s i 2s i", b"maya", 10, b"john", 30)

for x in struct.iter_unpack("3s i",data):
    print(f"{x}")
```

    (b'ma\x00', 10)
    (b'jo\x00', 30)



```python
data = struct.pack("4s i 2s i", b"maya", 10, b"john", 30)

for x in struct.iter_unpack("3s i",data):
    print(f"{x}")
```

    (b'may', 10)
    (b'jo\x00', 30)



```python
data = struct.pack("2s i 4s i", b"maya", 10, b"john", 30)

for x in struct.iter_unpack("4s i",data):
    print(f"{x}")
```

    (b'ma\x00\x00', 10)
    (b'john', 30)


#### `struct.pack_into`

Identical to the pack_into() function, using the compiled format.


```python
help(struct.pack_into)
```

    Help on built-in function pack_into in module _struct:
    
    pack_into(...)
        pack_into(format, buffer, offset, v1, v2, ...)
        
        Pack the values v1, v2, ... according to the format string and write
        the packed bytes into the writable buffer buf starting at offset.  Note
        that the offset is a required argument.  See help(struct) for more
        on format strings.
    



```python
import struct

ba2 = bytearray(2)

print(ba2, f"The size: {ba2.__len__()}")
struct.pack_into("bb", ba2, 0, 1, 1)

print(struct.unpack("bb", ba2))
```

    bytearray(b'\x00\x00') The size: 2
    (1, 1)



```python
import struct

ba2 = bytearray(5)

lst = [1, 1, 0, 1]
print(ba2, f"The size: {ba2.__len__()}")
struct.pack_into("bbbb", ba2, 1, *lst)
print(ba2)
print(struct.unpack("bbbbb", ba2))
```

    bytearray(b'\x00\x00\x00\x00\x00') The size: 5
    bytearray(b'\x00\x01\x01\x00\x01')
    (0, 1, 1, 0, 1)


#### `struct.unpack_from`

Identical to the unpack_from() function, using the compiled format.

### Understanding various errors


```python
import struct

ba2 = bytearray(4)

try:
    lst = [1, 1, 0, 1]
    print(ba2, f"The size: {ba2.__len__()}")
    struct.pack_into("bbbb", ba2, 1, *lst)
    print(ba2)
    print(struct.unpack("bbbbb", ba2))
except Exception as e:
    print(f"Error: {e}")
```

    bytearray(b'\x00\x00\x00\x00') The size: 4
    Error: pack_into requires a buffer of at least 5 bytes for packing 4 bytes at offset 1 (actual buffer size is 4)



```python
import struct

ba2 = bytearray(5) # size needed is offset (1) + size of lst (4)

try:
    lst = [1, 1, 0, 1]
    print(ba2, f"The size: {ba2.__len__()}")
    struct.pack_into("bbbb", ba2, 1, *lst)  
    print(ba2)
    print(struct.unpack("bbbbb", ba2))
except Exception as e:
    print(f"Error: {e}")
```

    bytearray(b'\x00\x00\x00\x00\x00') The size: 5
    bytearray(b'\x00\x01\x01\x00\x01')
    (0, 1, 1, 0, 1)



```python
import struct

ba2 = bytearray(5) 

try:
    lst = [1, 1, 0, 1]
    print(ba2, f"The size: {ba2.__len__()}")
    # We have 4 elements in the list, thus it should be 4 not 3 `b`
    struct.pack_into("bbb", ba2, 1, *lst)  
    print(ba2)
    print(struct.unpack("bbbbb", ba2))
except Exception as e:
    print(f"Error: {e}")
```

    bytearray(b'\x00\x00\x00\x00\x00') The size: 5
    Error: pack_into expected 3 items for packing (got 4)



```python
import struct
import sys

ba2 = bytearray(5)

try:
    lst = [1, 1, 0, 1]
    print(ba2, f"The size: {ba2.__len__()}")
    struct.pack_into("bbbb", ba2, 1, *lst)  # size needed is offset + size of lst
    print(ba2)
    # size needed is offset + size of lst thus need 5`b`
    print(struct.unpack("bbbb", ba2))
except Exception as e:
    tb = sys.exc_info()[2]
    print(f"Error {e} on line {tb.tb_lineno}, {tb}")
```

    bytearray(b'\x00\x00\x00\x00\x00') The size: 5
    bytearray(b'\x00\x01\x01\x00\x01')
    Error unpack requires a buffer of 4 bytes on line 12, <traceback object at 0x7efe6538ae40>



```python
print("welcome")
```

### codecs — Codec registry and base classes

$$𝑇𝑂𝐷𝑂$$
