## `Enum`

Just like older languages implementation, its a set of symbolic names (members) bound to unique, constant values. Within an enumeration, the members can be compared by identity, and the enumeration itself can be iterated over.

An enumeration is a set of symbolic names (members) bound to unique, constant values.

The `enum` provides four enumeration classes to define unique sets of names and values: 
- `Enum`, 
- `IntEnum`, 
- `Flag`,
- `IntFlag`
- Helper Function (`auto`)
- Decorator
    - `unique()`,

### Creating an Enum


```python
from enum import Enum

class Week(Enum):
    Sun = 1
    Mon = 2
    Tues = 3
    Wed = 4
    Thus = 5
    Fri = 6
    Sat = 7
```


```python
from enum import IntEnum

class TrafficLights(IntEnum): 
    red = 1
    yellow = 2 
    green = 3
```


```python
from enum import Flag

class Switch(Flag):
    On = True
    Off = False
```


```python
# using Auto

from enum import auto

class Colors(Enum): 
    GREEN = auto
    YELLOW = auto 
    BLUE = auto
```


```python
green = Colors(Colors.GREEN)
print(f"{green = }")
```

    green = <Colors.GREEN: <class 'enum.auto'>>


### Using an Enum


```python
print(f"{Week.Sun=}")
print(f"{Week.Mon.name=}")
print(f"{Week.Mon.value=}")
```

    Week.Sun=<Week.Sun: 1>
    Week.Mon.name='Mon'
    Week.Mon.value=2



```python
print(f"{green.name = } - {green.value = }")
```

    green.name = 'GREEN' - green.value = <class 'enum.auto'>

