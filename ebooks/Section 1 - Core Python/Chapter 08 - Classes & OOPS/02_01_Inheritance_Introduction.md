# OOPS Fundamentals 
----
## What is Inheritance?

Inheritance is used to indicate that one class will get most or all of its features from a parent class. This happens implicitly whenever you write class Foo(Bar), which says "Make a class Foo that inherits from Bar." When you do this, the language makes any action that you do on instances of Foo also work as if they were done to an instance of Bar. Doing this lets you put common functionality in the Bar class, then specialize that functionality in the Foo class as needed.

When you are doing this kind of specialization, there are three ways that the parent and child classes can interact:

* Actions on the child imply an action on the parent.
* Actions on the child override the action on the parent.
* Actions on the child alter the action on the parent.

Also to note:

- **Implicit Inheritance**: when you define a function in the parent, but not in the child. 
- **Override Explicitly**: when you define a function in the parent, and also in the child. 


```python
# Example 1:

class Parent(object):
    def __init__(self):
        print("Parent init")

class Child(Parent):
    def __init__(self):
        print("Child init")

obj = Child()
```

    Child init



```python
# Example 2: Child missing attribute __init__ 
# Thus using parent __init__

class Parent(object):
    def __init__(self):
        print("Ja, Parent init")

class Child(Parent):
    pass


obj = Child()
```

    Ja, Parent init



```python
class Parent(object):
    def __init__(self):
        print("Parent init")

    def everyone(self):
        print( "PARENT everyone()")

    def parent_only(self):
        print ("PARENT parent_only()")


class Child(Parent):
    def __init__(self):
        print("Child init")
        
    def everyone(self):
        print ("CHILD everyone()")
```


```python
child = Child()
```

    Child init



```python
child.parent_only()
```

    PARENT parent_only()



```python
child.everyone()
```

    CHILD everyone()



```python
class Parent(object):
    def __init__(self):
        print("Parent init")

    def everyone(self, x=0):
        self.x = x
        print(f"PARENT everyone() {x}")

    def parent_only(self, y):
        self.y = y
        print (f"PARENT parent_only() {y}")


class Child(Parent):
    def __init__(self):
        print("Child init")
        
    def everyone(self, x=12):
        self.x = x
        print (f"CHILD everyone() {x}")
```


```python
c, d = Child(), Child()
```

    Child init
    Child init



```python
c.everyone(100)
```

    CHILD everyone() 100



```python
print(c.__dict__)
print(d.__dict__)
```

    {'x': 100}
    {}



```python
c.parent_only(22)
```

    PARENT parent_only() 22



```python
print(c.__dict__)
print(d.__dict__)
```

    {'x': 100, 'y': 22}
    {}



```python
try:
    print(d.y)
except Exception as e:
    print(e)
```

    'Child' object has no attribute 'y'



```python
d.parent_only(20)

try:
    print(d.y)
except Exception as e:
    print(e)
```

    PARENT parent_only() 20
    20



```python
# Another Bad example. 

class Parent:
    x = [10]   # Reason for it being a bad example.
               # as we wanted `x` to be an instance variable

    def update(self, val):
        self.x.append(val)
    

class Child(Parent):
    def altered(self):
        p = super(Child, self)
        print(type(p))
        print ("CHILD, BEFORE PARENT altered()")
        p.altered()
        print ("CHILD, AFTER PARENT altered()")
```


```python
child1 = Child()
child2 = Child()

print(f"{child1.x = }, {id(child1.x) = }")
print(f"{child2.x = }, {id(child2.x) = }")
```

    child1.x = [10], id(child1.x) = 139922922949440
    child2.x = [10], id(child2.x) = 139922922949440



```python
child1.x.append(2000)
```


```python
print(f"{child1.x = }, {id(child1.x) = }")
print(f"{child2.x = }, {id(child2.x) = }")
```

    child1.x = [10, 2000, 2000], id(child1.x) = 139922922949440
    child2.x = [10, 2000, 2000], id(child2.x) = 139922922949440



```python
child1.x = "test"

print(f"{child1.x = }, {id(child1.x) = }")
print(f"{child2.x = }, {id(child2.x) = }")
```

    child1.x = 'test', id(child1.x) = 139692053596704
    child2.x = [10, 2000], id(child2.x) = 139691971264192



```python
# # Bit better example but not good examples. 

class Parent:
    def __init__(self, val):
        self.x = [val]
    
    def update(self, val):
        self.x.append(val)
    
class Child(Parent):
    def __init__(self):
        pass
        # super(Child, self).__init__()

    def altered(self):
        print(type(self.p))
        print ("CHILD, BEFORE PARENT altered()")
```


```python
child1 = Child()
child2 = Child()
```


```python
try:
    child1.update(100)
except Exception as e:
    print(f"Error: {e=}")
```

Opps, how to solve it. Since the parent `__init__` was never executed thus `x` variable was never created, hence the above exception.


```python
# Another example for the same problem.
# Problem of seperate init

class Parent:
    def __init__(self, title):
        print("Parent Init")
        self.title = tile
    
    def display(self, val):
        print("In parent display")
        print(self.title, val)

class Child(Parent):
    def __init__(self, name):
        print("Child init")
        self.name = name
        
    def username(self):
        print(self.name)
```


```python
try:
    ch = Child("Rahul")
    ch.display("Johri")
except Exception as e:
    print(e)
```

    Child init
    In parent display
    'Child' object has no attribute 'title'



```python
# Problem of seperate init

class Parent:
    def __init__(self, title):
        print("Running parent __init__")
        self.title = title
    
    def display(self):
        print(self.title)
        
    def username(self):
        print("In parent username")
    
class Child(Parent):
    def __init__(self, name, title):
        print("Running Child __init__")
        self.name = name
        
        # It will run the method from the next in line parent 
        super(Child, self).__init__(title)
        
    def username(self):
        """
        Solution to call parent function explicitly. 
        """
        print(self.name)
        super(Child, self).username()
```


```python
child1 = Child("Roshan", "MSI Interview Questions")
```

    Running Child __init__
    Running parent __init__



```python
child2 = Child("Anuja", "AI and Us")
```

    Running Child __init__
    Running parent __init__



```python
child1.display()
```

    MSI Interview Questions



```python
child2.username()
```

    Anuja
    In parent username



```python
child2.display()
```

    AI and Us


#### Immutable data


```python
# Bit better example but not good examples. 

class Parent:
    x = 10
    def override(self):
        print( "PARENT override()")

    def altered(self):
        print ("PARENT altered()")
    
    def update(self, val):
        self.x = val
    
class Child(Parent):

    def override(self):
        print ("CHILD override()")

    def altered(self):
        p = super(Child, self)
        print(type(p))
        print ("CHILD, BEFORE PARENT altered()")
        p.altered()
        print ("CHILD, AFTER PARENT altered()")

dad = Parent()
child1 = Child()
child2 = Child()

child1.update(100)
print(child1.x)
print(child2.x)
```

    100
    10



```python
child2.altered()
```

    <class 'super'>
    CHILD, BEFORE PARENT altered()
    PARENT altered()
    CHILD, AFTER PARENT altered()



```python
class Parent:
    def __init__(self):
        self.x = 10
    
    def update(self, val):
        self.x = val
    
class Child(Parent):
    def __init__(self):
        pass
    
    def altered(self, val):
        p = super(Child, self)
        p.update(val)

dad = Parent()
child1 = Child()
child2 = Child()

child1.altered(100)
print(child1.x)
print(child2.x)
```

    100



    ---------------------------------------------------------------------------

    AttributeError                            Traceback (most recent call last)

    <ipython-input-31-bc313223155e> in <module>
         20 child1.altered(100)
         21 print(child1.x)
    ---> 22 print(child2.x)
    

    AttributeError: 'Child' object has no attribute 'x'


## The Reason for `super()`

This should seem like common sense, but then we get into trouble with a thing called multiple inheritance. Multiple inheritance is when you define a class that inherits from one or more classes, like this:
```python
class SuperFun(Child, BadStuff):
    pass
```

This is like saying, "Make a class named SuperFun that inherits from the classes Child and BadStuff at the same time."

In this case, whenever you have implicit actions on any SuperFun instance, Python has to look-up the possible function in the class hierarchy for both Child and BadStuff, but it needs to do this in a consistent order. To do this Python uses "method resolution order" (MRO) and an algorithm called C3 to get it straight.

Because the MRO is complex and a well-defined algorithm is used, Python can't leave it to you to get the MRO right. Instead, Python gives you the super() function, which handles all of this for you in the places that you need the altering type of actions as I did in Child.altered. With super() you don't have to worry about getting this right, and Python will find the right function for you.

### Using super() with __init__
The most common use of super() is actually in __init__ functions in base classes. This is usually the only place where you need to do some things in a child, then complete the initialization in the parent. Here's a quick example of doing that in the Child:

```python
class Child(Parent):

    def __init__(self, stuff):
        self.stuff = stuff
        super(Child, self).__init__()
```
This is pretty much the same as the Child.altered example above, except I'm setting some variables in the __init__ before having the Parent initialize with its Parent.__init__.


```python
class Parent(object):
    def __init__(self, x):
        self.x = 10
    
    def update(self, val):
        self.x = val
    
class Child(Parent):
    def __init__(self, x):
        self.p = super(Child, self)
         
    def altered(self, val):
        self.p.update(val)


child = Child(19)
child.altered(100)
print(child.x)
```

    100



```python
c = Child(10)
print(f"{c = }")
```

    c = <__main__.Child object at 0x7f0c90481790>


## Chapter Appendix

### <img style="float:left;width:4em;height:4em;margin-left: unset;margin-right: 1em;margin-top: unset;" src="../../imgs/Quests.png"> Quests

#### What is a Class

#### Explain the characteristics of Objects

#### Define a protected member in a class

#### Is `class` mutable or immutable

#### What is the difference between @staticmethod and @classmethod in Python?

#### Explain overload constructors or methods

#### What all values `__init__` function can return

#### In a class, what `__ init__` function is used for

#### How to get the details of an object for a custom class

#### How to compare two objects of the same class

#### Explain Inheritance in Python
