## Enum

Just like older languages implementation, its a set of symbolic names (members) bound to unique, constant values. Within an enumeration, the members can be compared by identity, and the enumeration itself can be iterated over.

The `enum` provides four enumeration classes to define unique sets of names and values: 
- `Enum`, 
- `IntEnum`, 
- `Flag`,
- `IntFlag`
- Helper Function (`auto`)
- Decorator
    - `unique()`,


### `Enum`


```python
from enum import Enum

class TrafficLights(Enum): 
    red = 1
    yellow = 2 
    green = 3
    
    
tl = TrafficLights
print(f"{tl.red = }, {tl.red.name = }, {tl.red.value = }")
```

    tl.red = <TrafficLights.red: 1>, tl.red.name = 'red', tl.red.value = 1



```python
print(dir(tl))
```

    ['__class__', '__contains__', '__doc__', '__getitem__', '__init_subclass__', '__iter__', '__len__', '__members__', '__module__', '__name__', '__qualname__', 'green', 'red', 'yellow']



```python
print(dir(tl.red))
```

    ['__class__', '__doc__', '__eq__', '__hash__', '__module__', 'name', 'value']



```python
tl.red == 1
```




    False




```python
tl.red.value == 1
```




    True




```python
td = TrafficLights
```


```python
td.red == tl.red
```




    True


