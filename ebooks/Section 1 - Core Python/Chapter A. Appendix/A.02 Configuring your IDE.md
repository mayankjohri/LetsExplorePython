## A.02 Configuring your IDE

### VIM Editor

vi is a screen-oriented text editor originally created for the Unix operating system. The portable subset of the behavior of vi and programs based on it, and the ex editor language supported within these programs, is described by the Single Unix Specification and POSIX.

### Plugins to Install

#### Core Formatting

#### Syntax Highlighting

#### Color Themes

#### Project Navigation

#### Powerful Full Text search

#### Real time linting

- dense-analysis/ale 
- Coc (Conquer of Completion)


```python

```

#### Fixing


```python

```

#### Code Completion

We have following modules which can be used for code completion for python language. 

- Youcompleteme
- nvim-completion-manager (https://github.com/ncm2/ncm2)
- deoplete
- LanguageClient-neovim
- Aynccomplete
- Asyncomplete-lsp
- Jedi
- Jedi-vim
- rope
- ALE
- ....

They are all equally good and I will try to cover the installation & configuration process of few major ones.

##### `'davidhalter/jedi-vim'`
- Website: https://github.com/davidhalter/jedi-vim
- Installation command: `Plug 'davidhalter/jedi-vim'`

For default  behavior you don't have to add any other configuration, but it will still be a good idea to visit its website once.
![images/jedi-vim.png](images/jedi-vim.png)


```python

```

##### deoplate
- Website: https://github.com/deoplete-plugins/deoplete-jedi
- Installation command: Please find the below instructions

In order to install deoplate for python, we need to install following plugins
```vimrc
 Plug 'Shougo/deoplete.nvim'
 Plug 'roxma/nvim-yarp'
 Plug 'roxma/vim-hug-neovim-rpc'
 Plug 'deoplete-plugins/deoplete-jedi'
```
Also we need to install following python modules 
```bash
pip install --user pynvim jedi
```
Also start the deoplate at the start of the vim session using the following code in `vimrc` file

```
let g:deoplete#enable_at_startup = 1
```


If python is not installed in proper place or wish to use another python version then please specify them in the vimrc file
```
let g:python_host_prog  = '/usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'
```

![images/deoplate.png](images/deoplate.png)

#### Indentation

- Vimjas/vim-python-pep8-indent 

#### Folding

Folding (:help foldmethod) is when you collapse chunks of code to eliminate distraction.

The best approximation is to use the folding method indent though it doesn't work ideally.

au BufNewFile,BufRead *.py \
  set foldmethod=indent
To toggle a fold you can press za (:help fold-commands), and I have it mapped to Space for convenience.

nnoremap <space> za

#### Code Auto Styling

#### Code navigation


```python
CtrlP - File Searching
```

#### Code refactoring

#### Snippets

#### Git Integration


```python

```


```python

```


```python

```

#### All in One

- https://github.com/python-mode/python-mode


```python

```


```python

```


```python

```
