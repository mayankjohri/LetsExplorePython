## Ellipsis

The same as the ellipsis literal `...`. Special value used mostly in conjunction with extended slicing syntax for user-defined container data types. -- `Python Docs`

In this section we will cover where it can be used.

### Ellipsis As a Placeholder

Similar to `pass` we can use `Ellipsis` also as a placeholder as shown in the below code.


```python
def dummy_function():
    Ellipsis
```

or, we can use `...` format as shown below


```python
def dummy_function():
    ...
    
class C:
    def meth(self): 
        ...
```


```python
# Another example with placeholder data 

class User(object):
    name = ...
    def __init__(self, name =...) : ...
```


```python
user = User()
print(f"{user.name = }")
```

    user.name = Ellipsis



```python
# Proving that `...` is actually Ellipsis 

... == Ellipsis
```




    True



### Ellipsis in List and other data types - Circular references

`...` is also used to hide the dimensions in data as shown be below examples.


```python
# In List
lst = [1]
lst.append(lst)
print(lst)
```

    [1, [...]]



```python
y, z = [], []
x = [y, z]
y.append(z)
z.append(y)

print(f"{x = }, {y = } and {z = }")
```

    x = [[[[...]]], [[[...]]]], y = [[[...]]] and z = [[[...]]]



```python
# In dictionary
d = {}
d[0] = d
print(f"{d = }")
```

    d = {0: {...}}



```python
# Another Example for using Ellipsis 
# Removed most of the code for range except relevant code
# for understanding the use of `Ellipsis`

class myRange(object):
    def __getitem__(self, item):
        if item is Ellipsis:
            return "Everything returned"
        else:
            return f"{item} items returned"
        
x = myRange()
print (x[5])
print (x[...] )
```

    5 items returned
    Everything returned


### Ellipsis for Type Hinting

`Ellipsis` can be used in type hints as shown in the below examples

#### To denote variable-length homogeneous data in data collection


```python
from typing import Tuple, List

def UserCount( users: List[str, ...]): ...
    
def UserCount( users: List[str, ...]) -> Tuple(int, ...) : ...
```

### Funny Situations


```python
 print(....__class__.__name__)
```

    ellipsis


### Gotcha's

##### It can't be subclassed

We can't create subclasses using `Ellipsis`, trying it will always results in error as shown below


```python
# 1

try:
    class Spam(...): pass
except Exception as e:
    print(f"Error: {e}")
```

    Error: EllipsisType takes no arguments



```python
# 2

try:
    class Spam(type(...)): pass
except Exception as e:
    print(f"Error: {e}")
```

    Error: type 'ellipsis' is not an acceptable base type


##### Objects cannot be created using `Ellipsis`


```python
# 3

try:
    d = Ellipsis()
except Exception as e:
    print(f"Error: {e}") 
```

    Error: 'ellipsis' object is not callable


### References

- https://mail.python.org/pipermail/python-3000/2008-January/011792.html
- https://www.python.org/dev/peps/pep-0484/
