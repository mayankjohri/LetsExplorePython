""" Sample Code for testing lints """
import logging


def divide(first, second):
    """."""
    ilogger.info(f"Received {first = } and {second = } in func: divide")
    return first / second


def ve_msg(err_msg, func, *args, **kwargs):
    """."""

    try:
        print(err_msg)
        func(*args, **kwargs)
    except Exception as e:
        elogger.error(e, exc_info=True)


FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s"
logging.basicConfig(filename="logging.log", format=FORMAT, level=logging.DEBUG)

ilogger = logging.getLogger(__name__)
elogger = logging.getLogger("test_error")

divide(10, 2)

ve_msg("test", divide, 10, 10)
ve_msg("test", divide, 10, 0)
