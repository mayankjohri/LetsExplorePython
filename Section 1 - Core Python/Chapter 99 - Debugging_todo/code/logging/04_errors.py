import logging

logging.basicConfig(level=logging.INFO,
                    filename="simple_log.log",
                    filemode="w",
                    format="%(asctime)s %(levelname)s %(message)s")

a = 10
b = 0.0

try:
    c = a + b
    logging.info(f"value of {c} was result of {a} and {b}")
    d = a / b
except Exception as e:
    # logging.error(f"{e}", exc_info=True)
    logging.exception("error in code")
