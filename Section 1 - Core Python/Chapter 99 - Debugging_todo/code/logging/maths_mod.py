import logging

# Custom Logger
logger_1 = logging.getLogger(__name__)
logger_1.setLevel(logging.INFO)

handler1 = logging.FileHandler(f"{__name__}.log", mode='a')
formating = "%(name)s %(asctime)s %(levelname)s %(message)s"
formatter1 = logging.Formatter(formating)

# add formatter to the handler
handler1.setFormatter(formatter1)

# add handler to the logger
logger_1.addHandler(handler1)

logger_1.info(f"Testing the custom logger for module {__name__}...")

def test_division(x,y):
    try:
        x/y
        logger_1.info(f"x/y successful with result: {x/y}.")
    except ZeroDivisionError as err:
        logger_1.exception("ZeroDivisionError") 
