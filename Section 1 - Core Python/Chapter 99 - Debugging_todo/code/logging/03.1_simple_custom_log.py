import logging 


formatting = "%(asctime)s - %(levelname)s - %(filename)s::%(funcName)s - %(lineno)d |  %(message)s"
logging.basicConfig(level=logging.INFO,
                    filename="simple_log.log",
                    filemode="a",
                    format=formatting)

def info_message():
	logging.debug("A DEBUG Message")
	logging.info("An INFO message")

def error_warning():
	logging.warning("A WARNING message")
	logging.error("An ERROR message")

logging.info("---- let dance ----")
info_message()
error_warning()
logging.critical("A CRITICAL message")
logging.info("---- Bye Bye ----")
