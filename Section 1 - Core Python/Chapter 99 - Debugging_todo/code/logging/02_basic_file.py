import logging 

logging.basicConfig(level=logging.DEBUG,
                    filename="simple_log.log",
                    filemode="w")


logging.debug("A DEBUG Message")
logging.info("An INFO message")
logging.warning("A WARNING message")
logging.error("An ERROR message")
logging.critical("A CRITICAL message")
