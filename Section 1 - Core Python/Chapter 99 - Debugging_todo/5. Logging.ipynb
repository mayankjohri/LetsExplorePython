{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "04dcddbe",
   "metadata": {},
   "source": [
    "## Logging"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bf4efd0",
   "metadata": {},
   "source": [
    "Python provides a nice logging module which can handle most of our nornal logging requirements. `logging` module provides 5 types of log level.\n",
    "\n",
    "| Name       | Meaning                                                |\n",
    "|:-----------|:-------------------------------------------------------|\n",
    "| Debug      | Lowest level of logging, mostly used for debugging     |\n",
    "| Info       | To provide meaningful message about the progress       |\n",
    "| Warning    | Messages about possible issues or upcoming doom        |\n",
    "| Error      | Messages about the logical or system exceptions        |\n",
    "| Critical   | Messages about the doom or mission critical exceptions |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "ffc494e7",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "WARNING:root:A WARNING message\n",
      "ERROR:root:An ERROR message\n",
      "CRITICAL:root:A CRITICAL message\n"
     ]
    }
   ],
   "source": [
    "import logging\n",
    "\n",
    "logging.debug(\"A DEBUG Message\")\n",
    "logging.info(\"An INFO message\")\n",
    "logging.warning(\"A WARNING message\")\n",
    "logging.error(\"An ERROR message\")\n",
    "logging.critical(\"A CRITICAL message\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ad838bb",
   "metadata": {},
   "source": [
    "In the above example, by default only messages from \"WARNING\" and above are displayed, thus `DEBUG` and `INFO` messages are not displayed. \n",
    "\n",
    "The above behaviour can be changed through `basicConfig` function as shown below"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1c84db9",
   "metadata": {},
   "source": [
    "\n",
    "```python\n",
    "import logging \n",
    "\n",
    "logging.basicConfig(level=logging.INFO)\n",
    "\n",
    "logging.debug(\"A DEBUG Message\")\n",
    "logging.info(\"An INFO message\")\n",
    "logging.warning(\"A WARNING message\")\n",
    "logging.error(\"An ERROR message\")\n",
    "logging.critical(\"A CRITICAL message\")\n",
    "```\n",
    "**Output**:\n",
    "```python\n",
    "$ python basic.py \n",
    "INFO:root:An INFO message\n",
    "WARNING:root:A WARNING message\n",
    "ERROR:root:An ERROR message\n",
    "CRITICAL:root:A CRITICAL message\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffc203a4",
   "metadata": {},
   "source": [
    "### Log messages to a File"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93486d65",
   "metadata": {},
   "source": [
    "`logging.basicConfig` again comes to our rescue to log messages to the file, as it can be used to store the logging message to a file instead of console."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5994bbdd",
   "metadata": {},
   "source": [
    "\n",
    "```python\n",
    "import logging \n",
    "\n",
    "logging.basicConfig(level=logging.INFO, \n",
    "                    filename=\"simple_log.log\",\n",
    "                    filemode=\"w\")\n",
    "\n",
    "\n",
    "logging.debug(\"A DEBUG Message\")\n",
    "logging.info(\"An INFO message\")\n",
    "logging.warning(\"A WARNING message\")\n",
    "logging.error(\"An ERROR message\")\n",
    "logging.critical(\"A CRITICAL message\")\n",
    "```\n",
    "**Output**:\n",
    "```python \n",
    "$ python basic_file.py\n",
    "$ cat simple_log.log \n",
    "INFO:root:An INFO message\n",
    "WARNING:root:A WARNING message\n",
    "ERROR:root:An ERROR message\n",
    "CRITICAL:root:A CRITICAL message\n",
    " ```\n",
    "- Nothing is displayed on the console\n",
    "- all the messages following INFO have been stored in the log file `simple_log.log`\n",
    "- `filemode` \n",
    "    - `w` will override the logs from previous executions\n",
    "    - `a` will append the logs to existing messages."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3102665",
   "metadata": {},
   "source": [
    "### Custom Messages"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb8d6524",
   "metadata": {},
   "source": [
    "```python\n",
    "import logging \n",
    "\n",
    "logging.basicConfig(level=logging.INFO,\n",
    "\t\t    filename=\"simple_log.log\",\n",
    "\t\t    filemode=\"w\",\n",
    "            format=\"%(asctime)s %(levelname)s %(message)s\")\n",
    "\n",
    "\n",
    "logging.debug(\"A DEBUG Message\")\n",
    "logging.info(\"An INFO message\")\n",
    "logging.warning(\"A WARNING message\")\n",
    "logging.error(\"An ERROR message\")\n",
    "logging.critical(\"A CRITICAL message\")\n",
    "\n",
    "```\n",
    "\n",
    "**Output**:\n",
    "```python\n",
    "$ python 03_simple_custom_log.py\n",
    "$ cat simple_log.log \n",
    "2022-12-09 06:50:26,342 INFO An INFO message\n",
    "2022-12-09 06:50:26,342 WARNING A WARNING message\n",
    "2022-12-09 06:50:26,342 ERROR An ERROR message\n",
    "2022-12-09 06:50:26,342 CRITICAL A CRITICAL message\n",
    "```        "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42694850",
   "metadata": {},
   "source": [
    "We can also use the following attributes to customize our logging messages \n",
    "\n",
    "| Attribute name  | Format                                      | Description                                                  |\n",
    "|:--------------- |:------------------------------------------- |:------------------------------------------------------------ |\n",
    "| args            | You shouldn’t need to format this yourself. | The tuple of arguments merged into `msg` to produce `message`, or a dict whose values are used for the merge (when there is only one argument, and it is a dictionary). |\n",
    "| asctime         | `%(asctime)s`                               | Human-readable time when the [`LogRecord`](https://docs.python.org/3/library/logging.html#logging.LogRecord) was created.  By default this is of the form ‘2003-07-08 16:49:45,896’ (the numbers after the comma are millisecond portion of the time). |\n",
    "| created         | `%(created)f`                               | Time when the [`LogRecord`](https://docs.python.org/3/library/logging.html#logging.LogRecord) was created (as returned by [`time.time()`](https://docs.python.org/3/library/time.html#time.time)). |\n",
    "| exc_info        | You shouldn’t need to format this yourself. | Exception tuple (à la `sys.exc_info`) or, if no exception has occurred, `None`. |\n",
    "| filename        | `%(filename)s`                              | Filename portion of `pathname`.                              |\n",
    "| funcName        | `%(funcName)s`                              | Name of function containing the logging call.                |\n",
    "| levelname       | `%(levelname)s`                             | Text logging level for the message (`'DEBUG'`, `'INFO'`, `'WARNING'`, `'ERROR'`, `'CRITICAL'`). |\n",
    "| levelno         | `%(levelno)s`                               | Numeric logging level for the message (`DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`). |\n",
    "| lineno          | `%(lineno)d`                                | Source line number where the logging call was issued (if available). |\n",
    "| message         | `%(message)s`                               | The logged message, computed as `msg % args`. This is set when [`Formatter.format()`](https://docs.python.org/3/library/logging.html#logging.Formatter.format) is invoked. |\n",
    "| module          | `%(module)s`                                | Module (name portion of `filename`).                         |\n",
    "| msecs           | `%(msecs)d`                                 | Millisecond portion of the time when the [`LogRecord`](https://docs.python.org/3/library/logging.html#logging.LogRecord) was created. |\n",
    "| msg             | You shouldn’t need to format this yourself. | The format string passed in the original logging call. Merged with `args` to produce `message`, or an arbitrary object (see [Using arbitrary objects as messages](https://docs.python.org/3/howto/logging.html#arbitrary-object-messages)). |\n",
    "| name            | `%(name)s`                                  | Name of the logger used to log the call.                     |\n",
    "| pathname        | `%(pathname)s`                              | Full pathname of the source file where the logging call was issued (if available). |\n",
    "| process         | `%(process)d`                               | Process ID (if available).                                   |\n",
    "| processName     | `%(processName)s`                           | Process name (if available).                                 |\n",
    "| relativeCreated | `%(relativeCreated)d`                       | Time in milliseconds when the LogRecord was created, relative to the time the logging module was loaded. |\n",
    "| stack_info      | You shouldn’t need to format this yourself. | Stack frame information (where available) from the bottom of the stack in the current thread, up to and including the stack frame of the logging call which resulted in the creation of this record. |\n",
    "| thread          | `%(thread)d`                                | Thread ID (if available).                                    |\n",
    "| threadName      | `%(threadName)s`                            | Thread name (if available).                                  |\n",
    "\n",
    "Changed in version 3.1: *processName* was added.\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "915ebada",
   "metadata": {},
   "source": [
    "### Logging errors and its details"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bfe71192",
   "metadata": {},
   "source": [
    "```python\n",
    "# 04_errors.py\n",
    "import logging \n",
    "\n",
    "logging.basicConfig(level=logging.INFO,\n",
    "\t\t    filename=\"simple_log.log\",\n",
    "\t\t    filemode=\"w\",\n",
    "                    format=\"%(asctime)s %(levelname)s %(message)s\")\n",
    "\n",
    "a = 10\n",
    "b = 0.0\n",
    "\n",
    "try:\n",
    "    c = a + b\n",
    "    logging.info(f\"value of {c} was result of {a} and {b}\")\n",
    "    d = a / b\n",
    "except Exception as e:\n",
    "    logging.error(f\"{e}\", exc_info=True)\n",
    "    # logging.exception(\"error message\")\n",
    "\n",
    "```\n",
    "\n",
    "**Output**:\n",
    "```python\n",
    "2022-12-09 07:02:33,620 INFO value of 10.0 was result of 10 and 0.0\n",
    "2022-12-09 07:02:33,620 ERROR float division by zero\n",
    "Traceback (most recent call last):\n",
    "  File \"/home/mayank/code/mj/ebooks/python/lep/Section 1 - Core Python/Chapter 99 - Debugging_todo/code/logging/04_errors.py\", line 14, in <module>\n",
    "    d = a / b\n",
    "ZeroDivisionError: float division by zero\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e5c44cf",
   "metadata": {},
   "source": [
    "```python\n",
    "import logging \n",
    "\n",
    "\n",
    "formatting = \"%(asctime)s - %(levelname)s - %(filename)s::%(funcName)s - %(lineno)d  -  %(message)s\"\n",
    "logging.basicConfig(level=logging.INFO,\n",
    "                    filename=\"simple_log.log\",\n",
    "                    filemode=\"a\",\n",
    "                    format=formatting)\n",
    "\n",
    "def info_message():\n",
    "    logging.debug(\"A DEBUG Message\")\n",
    "    logging.info(\"An INFO message\")\n",
    "\n",
    "def error_warning():\n",
    "    logging.warning(\"A WARNING message\")\n",
    "    logging.error(\"An ERROR message\")\n",
    "\n",
    "logging.info(\"---- let dance ----\")\n",
    "info_message()\n",
    "error_warning()\n",
    "logging.critical(\"A CRITICAL message\")\n",
    "logging.info(\"---- Bye Bye ----\")\n",
    "\n",
    "```\n",
    "\n",
    "**Output**:\n",
    "```python\n",
    "2022-12-09 09:21:47,607 - INFO - 03.1_simple_custom_log.py::<module> - 18  -  ---- let dance ----\n",
    "2022-12-09 09:21:47,607 - INFO - 03.1_simple_custom_log.py::info_message - 12  -  An INFO message\n",
    "2022-12-09 09:21:47,607 - WARNING - 03.1_simple_custom_log.py::error_warning - 15  -  A WARNING message\n",
    "2022-12-09 09:21:47,607 - ERROR - 03.1_simple_custom_log.py::error_warning - 16  -  An ERROR message\n",
    "2022-12-09 09:21:47,608 - CRITICAL - 03.1_simple_custom_log.py::<module> - 21  -  A CRITICAL message\n",
    "2022-12-09 09:21:47,608 - INFO - 03.1_simple_custom_log.py::<module> - 22  -  ---- Bye Bye ----\n",
    "\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb88a244",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "e6147afb",
   "metadata": {},
   "source": [
    "```python\n",
    "import logging \n",
    "\n",
    "logging.basicConfig(level=logging.INFO,\n",
    "\t\t    filename=\"simple_log.log\",\n",
    "\t\t    filemode=\"w\",\n",
    "                    format=\"%(asctime)s %(levelname)s %(message)s\")\n",
    "\n",
    "a = 10\n",
    "b = 0.0\n",
    "\n",
    "try:\n",
    "    c = a + b\n",
    "    logging.info(f\"value of {c} was result of {a} and {b}\")\n",
    "    d = a / b\n",
    "except Exception as e:\n",
    "    # logging.error(f\"{e}\", exc_info=True)\n",
    "    logging.exception(\"error in code\")\n",
    "\n",
    "```\n",
    "\n",
    "**Output**:\n",
    "```python\n",
    "2022-12-09 09:25:09,016 INFO value of 10.0 was result of 10 and 0.0\n",
    "2022-12-09 09:25:09,016 ERROR error in code\n",
    "Traceback (most recent call last):\n",
    "  File \"/home/mayank/code/mj/ebooks/python/lep/Section 1 - Core Python/Chapter 99 - Debugging_todo/code/logging/04_errors.py\", line 14, in <module>\n",
    "    d = a / b\n",
    "ZeroDivisionError: float division by zero\n",
    "\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0fd1c360",
   "metadata": {},
   "source": [
    "### Custom Loggers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad2d7150",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39f8e65b",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "6ae7e903",
   "metadata": {},
   "outputs": [
    {
     "ename": "SyntaxError",
     "evalue": "leading zeros in decimal integer literals are not permitted; use an 0o prefix for octal integers (2953890744.py, line 7)",
     "output_type": "error",
     "traceback": [
      "\u001b[0;36m  Cell \u001b[0;32mIn [18], line 7\u001b[0;36m\u001b[0m\n\u001b[0;31m    2022-12-09 07:02:33,620 INFO value of 10.0 was result of 10 and 0.0\u001b[0m\n\u001b[0m            ^\u001b[0m\n\u001b[0;31mSyntaxError\u001b[0m\u001b[0;31m:\u001b[0m leading zeros in decimal integer literals are not permitted; use an 0o prefix for octal integers\n"
     ]
    }
   ],
   "source": [
    "```python\n",
    "\n",
    "```\n",
    "\n",
    "**Output**:\n",
    "```python\n",
    "2022-12-09 07:02:33,620 INFO value of 10.0 was result of 10 and 0.0\n",
    "2022-12-09 07:02:33,620 ERROR float division by zero\n",
    "Traceback (most recent call last):\n",
    "  File \"/home/mayank/code/mj/ebooks/python/lep/Section 1 - Core Python/Chapter 99 - Debugging_todo/code/logging/04_errors.py\", line 14, in <module>\n",
    "    d = a / b\n",
    "ZeroDivisionError: float division by zero\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32cacc0f",
   "metadata": {},
   "source": [
    "### Best Practices"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f5f8e68",
   "metadata": {},
   "source": [
    "- Set the optimal logging level\n",
    "- Configure loggers at the module level\n",
    "- Include timestamps and ensure consistent formatting\n",
    "- Rotate the log files"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0a2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
