# Preface
---

This book has been created as part of reference material for learning Python languages.

The entire training material has been divided into three sections

* Core Python
* Advance Python & (**Part 2**)
* Machine Learning (**Part 3**)

This e-book will try to cover only "**core python**" and I am in a process of finishing the other two e-books. I am planning to release the First (draft) versions of them with following time lines. 

- "Lets Explore Advance Python" might be released sometime in March 2023
- "Lets Explore Machine Learning using Python" by Dec 2023
- "Frequently Asked Interview Questions in ML (Python)" by June 2023

This section is specifically designed for everyone who wish to work/learn Python language.

### Note

Few chapter names are ending with "**\_todo**", that denotes that I am still working on those chapters and might be finished in future versions and are NOT completed in this version. Few might not even have started in current version.

## Reference

Below is the list of reference materials which I use, when conducting Python training and also materials from my Web site that are intended for self-instruction.

Most of the material has been collected from Internet. Primary source of the information in the book has been from the followings.

1. My Understanding of Python

2. Python Manual http://docs.python.org

3. [https://github.com/ricardoduarte/python-for-developers](https://github.com/ricardoduarte/python-for-developers)

4. [http://www.davekuhlman.org/python\_book\_01.pdf](http://www.davekuhlman.org/python_book_01.pdf)

5. Wikipedia

6. Wiki-books, etc




<iframe srcdoc="&lt;html&gt;
    &lt;body&gt;
        &lt;script src=&quot;https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js&quot;&gt;&lt;/script&gt;
        &lt;script&gt;
            mermaid.initialize({ startOnLoad: true });
        &lt;/script&gt;

        &lt;div class=&quot;mermaid&quot;&gt;
            flowchart LR
    Root --&gt; A[Apps]
    Root --&gt; T[Tests]
    A[Apps] --&gt; B(App Folder &amp; Data)
    T[Tests] --&gt; f[Features] --&gt; tf(test.feature)
    f[Features] --&gt; mf(mul.feature)
    f[Features] --&gt; S[Steps] --&gt; as(add_steps.py)
    S[Steps] --&gt; ms(mul_steps.py)
        &lt;/div&gt;

    &lt;/body&gt;
&lt;/html&gt;
" width="100%" height="280"style="border:none !important;" "allowfullscreen" "webkitallowfullscreen" "mozallowfullscreen"></iframe>


