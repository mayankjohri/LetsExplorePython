### String Module

Various functions for dealing with text are implemented in the module *string*.


```python
import string

# the alphabet
print(dir(string))
```

    ['Formatter', 'Template', '_ChainMap', '__all__', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '_re', '_sentinel_dict', '_string', 'ascii_letters', 'ascii_lowercase', 'ascii_uppercase', 'capwords', 'digits', 'hexdigits', 'octdigits', 'printable', 'punctuation', 'whitespace']



```python
a = string.ascii_letters
print(a)
```

    abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ



```python
print(string.ascii_lowercase)
```

    abcdefghijklmnopqrstuvwxyz



```python
print(string.ascii_uppercase)
```

    ABCDEFGHIJKLMNOPQRSTUVWXYZ



```python
print(string.digits)
```

    0123456789



```python
print(string.hexdigits)
```

    0123456789abcdefABCDEF



```python
print(string.octdigits)
```

    01234567



```python
print(string.printable)
```

    0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ 	
    



```python
print(string.punctuation)
```

    !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~



```python
string.capwords("WElcome")
```




    'Welcome'




```python
string.capwords("""Create a new string object from the given object. If encoding or
errors is specified, then the object must expose a data buffer
that will be decoded using the given encoding and error handler.
Otherwise, returns the result of object.__str__() (if defined)
or repr(object).""")
```




    'Create A New String Object From The Given Object. If Encoding Or Errors Is Specified, Then The Object Must Expose A Data Buffer That Will Be Decoded Using The Given Encoding And Error Handler. Otherwise, Returns The Result Of Object.__str__() (if Defined) Or Repr(object).'




```python
print(string.digits)
print(string.hexdigits)
print(string.printable)
```

    0123456789
    0123456789abcdefABCDEF
    0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ 	
    



```python
print(string.whitespace)
```

     	
    

