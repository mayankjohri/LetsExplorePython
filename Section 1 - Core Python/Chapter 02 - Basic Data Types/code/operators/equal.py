a = [1, 2]
b = [4, 5]

print(id(b))

# its like `expand` operation on list. Here `__iadd__` method is called 
# instead of `__add__` method
b += a

print(b, a, id(b))
