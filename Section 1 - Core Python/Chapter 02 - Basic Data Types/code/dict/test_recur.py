
import pytest

from .nrec import get_val

data = {'a': {
    'b': {
        'd': 10
        }
    }
}



@pytest.mark.parametrize("key, exp_val, msg", 
                         [
                            ("a/b/d", 10, "Values not same"), 
                            ("a/b/d/f", "N/F", "Value Found")
                         ]
                        )
def test_val(key, exp_val, msg):
    got_val = get_val(data, key)
    assert got_val == exp_val, msg + f"Got_val: {got_val}, Exp: {exp_val}"

