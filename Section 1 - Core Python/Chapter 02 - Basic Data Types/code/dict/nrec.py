data = {
        'a': 212,
        'b': {
            'b1': 21
            },
        'c': {
            'c1': {
                'c_11' : 'test'
                }
            }
        }

keys = ['a', 'c/c1', 'c/c1/c_11', 'b/b1/b2', 'vd', 'a/b/c']



def get_val(data, key):
    from copy import deepcopy

    d = deepcopy(data)
    for k in key.split("/"):
        if isinstance(d, dict) and k in d:
            d = d[k]
        else:
            return "N/F"
    else:
        return d if k else d[key]

for key in keys:
    vals = get_val(data, key)
    print(key, ":", vals)
