# Generators

The functions generally follow the conventional process flow, return values ​​and quit. Generators work similarly, but remember the state of the processing between calls, staying in memory and returning the next item expected when activated.

The generators have several advantages over conventional functions:

+ *Lazy Evaluation*: generators are only processed when it is really needed, saving processing resources. 
+ They reduce the need to create lists.
+ They allow to work with unlimited sequences of elements.

Generators are usually called through a *for* loop. The  syntax is similar to the traditional function, just the *yield* instruction substitutes *return*. In each new iteraction, *yield* returns the next value.

**Example:**


```python
from time import sleep

def list_pares():
    """
    Function equivalient of the previous generator 
    Generate even numbers from 0 to 10
    """
    i = 0
    lst = []
    while i <= 5:
        #  print("in while loop")
        lst.append(i)
        sleep(1)
        i += 2
    print(f"returning the list: {lst = }")
    return lst
```


```python
print(list_pares())
```

    returning the list: lst = [0, 2, 4]
    [0, 2, 4]



```python
# using function to get the list

for n in list_pares():
    print ("In For loop > ", n)
```

    returning the list: lst = [0, 2, 4]
    In For loop >  0
    In For loop >  2
    In For loop >  4



```python
from time import sleep

def gen_pares():
    """
    Generates even numbers from 0 to 10
    """
    i = 0
    while i <= 5:
        print(f"before yield i: {i = }")
        sleep(1)
        yield i
        sleep(3)
        print(f"after yield i: {i = }")
        i += 2
```


```python
print(gen_pares())
```

    <generator object gen_pares at 0x7f4752d27370>



```python
print(dir(gen_pares()))
```

    ['__class__', '__del__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getstate__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__lt__', '__name__', '__ne__', '__new__', '__next__', '__qualname__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'close', 'gi_code', 'gi_frame', 'gi_running', 'gi_suspended', 'gi_yieldfrom', 'send', 'throw']



```python
# Bad Idea: as generator loses its benefit. 

# print(tuple(gen_pares()), end=", ")
# print(list(gen_pares()), end=", ")
```


```python
# Shows each number and goes to the next

for n in gen_pares():
    print ("In For loop > ", n)
```

    before yield i: i = 0
    In For loop >  0
    after yield i: i = 0
    before yield i: i = 2
    In For loop >  2
    after yield i: i = 2
    before yield i: i = 4
    In For loop >  4
    after yield i: i = 4



```python
# TO find : Generators and while
# Dont use it.

# while x:= gen_pares():
#     print(f"Hello {x=}")
```


```python
from time import sleep

def gen_pares():
    """
    Generator with multiple yields:
    Generator remembers its execution position 
    even after yield.
    """
    for x in range(5):
        print(f"yielding: x - {x}")
        yield x
        i = x + 2
        sleep(1.5)
        print(f"yielding: i - {i}")
        yield i
```


```python

# Shows each number and goes to the next
for n in gen_pares():
    print("> ", n)
```

    yielding: x - 0
    >  0
    yielding: i - 2
    >  2
    yielding: x - 1
    >  1
    yielding: i - 3
    >  3
    yielding: x - 2
    >  2
    yielding: i - 4
    >  4
    yielding: x - 3
    >  3
    yielding: i - 5
    >  5
    yielding: x - 4
    >  4
    yielding: i - 6
    >  6



```python
from time import sleep

def gen_pares():
    """
    Generator with multiple yields and a return:
    Generator remembers its execution position 
    even after yield.
    """
    for x in range(5):
        print(f"yielding: {x=}")
        yield x
        i = x + 2
        if x > 1:
            print(f"Return {x=}")
            return x
        sleep(0.5)
        print(f"yielding: {i=}")
        yield i

# Shows each number and goes to the next
for n in gen_pares():
    print("> ", n)
```

    yielding: x=0
    >  0
    yielding: i=2
    >  2
    yielding: x=1
    >  1
    yielding: i=3
    >  3
    yielding: x=2
    >  2
    Return x=2



```python
# `next` function can return the next value
# which the generator should return

my_gen_data = gen_pares()
print(my_gen_data)
print(f"{next(my_gen_data) = }")
print(f"{next(my_gen_data) = }")
print("---")
print(f"{next(my_gen_data) = }")
print(f"{next(my_gen_data) = }")
print(f"{next(my_gen_data) = }")
print(f"{next(my_gen_data) = }")
print(f"{next(my_gen_data) = }")
try:
    print(f"{next(my_gen_data) = }")
    print(f"{next(my_gen_data) = }")
    print(f"{next(my_gen_data) = }")
    print(f"{next(my_gen_data) = }")
    print(f"{next(my_gen_data) = }")
    print(f"{next(my_gen_data) = }")
    print(f"{next(my_gen_data) = }")
except StopIteration as e:
    print(f"Error: {e}")
```

    <generator object gen_pares at 0x7fb228260d60>
    yielding: x - 0
    next(my_gen_data) = 0
    yielding: i - 2
    next(my_gen_data) = 2
    ---
    yielding: x - 1
    next(my_gen_data) = 1
    yielding: i - 3
    next(my_gen_data) = 3
    yielding: x - 2
    next(my_gen_data) = 2
    yielding: i - 4
    next(my_gen_data) = 4
    yielding: x - 3
    next(my_gen_data) = 3
    yielding: i - 5
    next(my_gen_data) = 5
    yielding: x - 4
    next(my_gen_data) = 4
    yielding: i - 6
    next(my_gen_data) = 6
    Error: 


Another example:


```python
import os

# Finds files recursively
def find_files(path='.'):
    try:
        lst = []
        for item in os.listdir(path):
            fn = os.path.normpath(os.path.join(path, item))

            if os.path.isdir(fn):
                for f in find_files(fn):
                    lst.append(f)
            else:
                lst.append(fn)
        return lst
    except Exception as e:
        print("Error:", e)
        return lst
        

# At each interaction, the generator yeld a new file name
# for fn in find_files(r"/etc/periodic/daily/"):
for fn in find_files(r"/var/log"):
    print(fn, end=", ")
```

    Error: [Errno 13] Permission denied: '/var/log/kernel'
    Error: [Errno 13] Permission denied: '/var/log/sshd'
    Error: [Errno 13] Permission denied: '/var/log/pwdfail'
    Error: [Errno 13] Permission denied: '/var/log/nullmailer'
    Error: [Errno 13] Permission denied: '/var/log/everything'
    Error: [Errno 13] Permission denied: '/var/log/telnet'
    Error: [Errno 13] Permission denied: '/var/log/gdm'
    /var/log/.keep, /var/log/portage/cross-arm-none-eabi-binutils.log.xz, /var/log/portage/elog/summary.log, /var/log/portage/cross-arm-none-eabi-info.log, /var/log/portage/cross-arm-none-eabi-binutils.log, /var/log/Xorg.0.log.old, /var/log/lastlog, /var/log/wtmp, /var/log/genkernel.log, /var/log/emerge-fetch.log, /var/log/Xorg.1.log.old, /var/log/cups/access_log, /var/log/cups/error_log, /var/log/emerge.log, /var/log/dmesg, /var/log/Xorg.0.log, /var/log/Xorg.1.log, /var/log/tallylog, 


```python
import os

# Finds files recursively
def find_files(path='.'):
    try:
        for item in os.listdir(path):
            fn = os.path.normpath(os.path.join(path, item))

            if os.path.isdir(fn):
                for f in find_files(fn):
                    yield f
            else:
                yield fn
    except Exception as e:
        pass

# At each interaction, the generator yeld a new file name
for fn in find_files(r"/var/log"):
    print(fn, end=", ")
```

    /var/log/.keep, /var/log/portage/cross-arm-none-eabi-binutils.log.xz, /var/log/portage/elog/summary.log, /var/log/portage/cross-arm-none-eabi-info.log, /var/log/portage/cross-arm-none-eabi-binutils.log, /var/log/Xorg.0.log.old, /var/log/lastlog, /var/log/wtmp, /var/log/genkernel.log, /var/log/emerge-fetch.log, /var/log/Xorg.1.log.old, /var/log/cups/access_log, /var/log/cups/error_log, /var/log/emerge.log, /var/log/dmesg, /var/log/Xorg.0.log, /var/log/Xorg.1.log, /var/log/tallylog, 


```python
from time import sleep
import sys

def fib():
    a, b = 0, 1
    while True:
        sleep(0.5)
        yield b
        d = a + b
        if d > 20:
            break
        a, b = b, d 

iter_fib = fib()

```


```python

try:
    for i in iter_fib:
        print(i, end=" ")
        sys.stdout.flush()
    else:
        print("...Done...")
except KeyboardInterrupt: 
    print( "Calculation stopped")
```

    1 1 2 3 5 8 13 ...Done...



```python
iter_fib = fib()
print(dir(iter_fib))
```

    ['__class__', '__del__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__lt__', '__name__', '__ne__', '__new__', '__next__', '__qualname__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'close', 'gi_code', 'gi_frame', 'gi_running', 'gi_yieldfrom', 'send', 'throw']



```python
# We can use `__next__` function also instead of next
try:
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    # At this point the exception is raised
    print(iter_fib.__next__())
except StopIteration as e:  
    # StopIteration is raised when generate 
    # have returned all the required data 
    print(f"Error: {e = }")
```

    Error: e = StopIteration()



```python
# We can use `__next__` function also instead of next
try:
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    print(iter_fib.__next__())
    # At this point the exception is raised
    print(iter_fib.__next__())
except StopIteration as e:  
    # StopIteration is raised when generate 
    # have returned all the required data 
    print(f"Error: {e = }")
```

    Error: e = StopIteration()



```python
# Sorry no luck :)
try:
    for a in range(10):
        raise(StopIteration)
    else:
        print("Done")
except Exception as e:
    print(f"Error: {e=}")
```

    Error: e=StopIteration()


### While loop and generators

Not a good combination, as `while` loop is not able to handle the `StopIteration` exception. As the `while` loop checks for the condition.


```python
iter_fib = fib()

try:
    while (iter_fib.__next__()):
        print("Ja, Ich bin ein Kind")
except StopIteration as e:
    print(f"Error: { e = }")
```

    Ja, Ich bin ein Kind
    Ja, Ich bin ein Kind
    Ja, Ich bin ein Kind
    Ja, Ich bin ein Kind
    Ja, Ich bin ein Kind
    Ja, Ich bin ein Kind
    Ja, Ich bin ein Kind
    Error:  e = StopIteration()



```python

iter_fib = fib()

try:
    while (next_val := iter_fib.__next__()):
        print(f"{next_val = }")
except StopIteration as e:
    print(f"Error: { e = }")
```

    next_val = 1
    next_val = 1
    next_val = 2
    next_val = 3
    next_val = 5
    next_val = 8
    next_val = 13
    Error:  e = StopIteration()

