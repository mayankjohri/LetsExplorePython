# Iterators

Iterators in Python are in abundance,
- All Generators
- Most Collection data type

Python iterator objects are required to support two methods while following the iterator protocol.

- `__iter__` returns the iterator object itself. This is used in for and in statements.

- `__next__` method returns the next value from the iterator. If there is no more items to return then it should raise `StopIteration` exception.

In this chapter we are going to cover only custom iterators as almost all other iterators have been taught in other sections.


```python
for a in range(10):
    print(a, end="  ")
```

    0  1  2  3  4  5  6  7  8  9  


```python
# What is it then ?????

print(dir(range))
```

    ['__bool__', '__class__', '__contains__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__reversed__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'count', 'index', 'start', 'step', 'stop']



```python
class MyRange(object):
    
    def __init__(self, ini_val=0, end_val=10, hop=1):
        self.__ini_val = ini_val
        self.__end_val = end_val
        self.__hop = hop
        self.__next_val = self.__ini_val
        
    def __iter__(self):
        return self
    
    def __next__(self):
        if self.__next_val >= self.__end_val:
            raise StopIteration("End has reached")
        else:
            val = self.__next_val
            self.__next_val += self.__hop 
            return val  # We dont need yield here.
```


```python
for a in MyRange(1, 3, 4):
    print(a, end=" ->")
print("\b\b")
```

    1 ->



```python
for a in MyRange(1, 22, 4):
    print(a, end=" ->")
print("\b\b")
```

    1 ->5 ->9 ->13 ->17 ->21 ->



```python
mt = MyRange(1, 4)

try:
    print(mt.__next__())
    print(mt.__next__())
    print(mt.__next__())
    print(mt.__next__(), ".")
except StopIteration as sie:
    print("Error:", sie)
```

    1
    2
    3
    Error: End has reached



```python
print(dir(MyRange))
```

    ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__next__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__']

