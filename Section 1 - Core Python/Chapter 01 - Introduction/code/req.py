#!/usr/bin/env python

mass_kg = int(input("What is your mass in kilograms? Please make sure that you provide only the correct value, currently  we do not handle it properly" ))
mass_stone = mass_kg * 2.2 / 14
print("You weigh", mass_stone, "stone.")
