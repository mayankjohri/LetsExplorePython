x = y = z = 1000

print(f"{x=}, {y=}, {z=}")

print(f"{x=} and {id(x)=}")
print(f"{y=} and {id(y)=}")
print(f"{z=} and {id(z)=}")

print("*"*20)

x = 1000
y = 1000
z = 1000
print(f"{x=} and {id(x)=}")
print(f"{y=} and {id(y)=}")
print(f"{z=} and {id(z)=}")
