# property 
-----
Python has a great concept called property, which makes the life of an object oriented programmer much simpler. Before defining and going into details of what a property in Python is, let us first build an intuition on why it would be needed in the first place.


```python
CONST = 9/5 # some constant

class Weather_balloon(object):
    def __init__(self, temp):
        self.temp = temp
    
    def convert_temp_to_f(self):
        return self.temp * CONST + 32
    

w = Weather_balloon(122)
print(w.convert_temp_to_f())
```

    251.6



```python
# Or we have interdependent attributes.
# In this example, we have radius and area, which are dependend or each other. 

class Circle(object):
    def __init__(self, radius):
        self.radius = radius
        self.area = 3.14*radius*radius
```


```python
c = Circle(10)
print(c.radius)
```

    10



```python
print(c.area)
```

    314.0



```python
# Oppppp's !!!!

# Lets change the area and see its effect on radius.
c.area = 222

# In this case it will be zero change. 
print(f"c.radius: {c.radius}")
print(f"c.area: {c.area}")
```

    c.radius: 10
    c.area: 222


#### Getters and Setters to the recue ?


```python
class Circle(object):
    def __init__(self, radius):
        self.radius = radius
        self.area = 3.14*radius*radius
        
    def set_radius(self, radius):
        self.radius = radius 
        self.area = 3.14*radius*radius
        
    def get_radius(self):
        return self.radius
```


```python
red_circle = Circle(10)


print(f"c.radius: {red_circle.radius}")
print(f"c.area: {red_circle.area}")
```

    c.radius: 10
    c.area: 314.0



```python
red_circle.set_radius(200)
print(red_circle.get_radius())

print(f"c.radius: {red_circle.radius}")
print(f"c.area: {red_circle.area}")
```

    200
    c.radius: 200
    c.area: 125600.0


I still have problem, that now I have to remeber function names twice the number of attributes. 

### Issue? 

Since they are interdependent attributes, changes one invalidates the other, thus we need to update both at the same time. Properties allows me to achieve it without any issue. 


```python
# Interdepended attributes area & radius
# Not converted _area to property
# leaving for the readers to create that similar to radius

class Circle(object):
    
    def __init__(self, radius):
        self.__set_radius(radius)
        # Gotcha 
#         self.radius = property(self.__get_radius, self.__set_radius)
        
    def get_area(self):
        return self._area
    
    def __get_radius(self):
        print("Getting the value of radius")
        return self.__radius
    
    def __set_radius(self, radius):
        print("__set_radius")
        self.__radius = radius
        self._area = 3.14*radius*radius

    radius = property(__get_radius, __set_radius)
```


```python
c = Circle(10)
```

    __set_radius



```python
# Properties are self aware and behave differently in different 
# context in which its called.

print(f"{c.radius = }")  # This will intern call `__get_radius` function
print(f"{c._area = }")

```

    Getting the value of radius
    c.radius = 10
    c._area = 314.0



```python
# Lets change the radius and see its impact on area.

c.radius = 100  # This will intern call `__set_radius` function
```

    __set_radius



```python
print(f"{c.radius = }")  # This will intern call `__get_radius` function
print(f"{c._area = }")
```

    Getting the value of radius
    c.radius = 100
    c._area = 31400.0



```python
# Interdepended attributes area & radius
# Not converted _area to property
# leaving for the readers to create that similar to radius
# Method 2
# using Blank property and populating it later

class Circle(object):
    
    def __init__(self, radius):
        self.__set_radius(radius)
        # Gotcha 
        # self.radius = property(self.__get_radius, self.__set_radius)
        # radius = radius.getter(dummy_get)
        
        
    def get_area(self):
        return self._area
    
    def dummy_get(self):
        return 10001
    
    def __get_radius(self):
        print("Getting the value of radius")
        return self.__radius
    
    def __set_radius(self, radius):
        print("__set_radius")
        self.__radius = radius
        self._area = 3.14*radius*radius

    radius = property()
    radius = radius.getter(__get_radius)
    radius = radius.setter(__set_radius)
```


```python
c = Circle(10)
```

    __set_radius



```python
# Properties are self aware and behave differently in different 
# context in which its called.

print(f"{c.radius = }")  # This will intern call `__get_radius` function
print(f"{c._area = }")
```

    Getting the value of radius
    c.radius = 10
    c._area = 314.0



```python
# Lets change the radius and see its impact on area.

c.radius = 100  # This will intern call `__set_radius` function
```

    __set_radius



```python
print(f"{c.radius = }")  # This will intern call `__get_radius` function
print(f"{c._area = }")
```

    Getting the value of radius
    c.radius = 100
    c._area = 31400.0



```python
# Using decorator to create a property. 

import math

class Circle(object):

    def __init__(self, radius):
        self.__set_radius(radius)
#         self.radius = property(self.__get_radius, 
#                                self.__set_radius)

    def __get_radius(self):
        return self.__radius
    
    def __set_radius(self, radius):
        self.__radius = radius
        self.__area = 3.14*radius*radius
    
    radius = property(__get_radius, 
                      __set_radius)
    
    @property
    def area(self):
        """
        Getter Method
        """
        return self.__area
    
    @area.setter
    def area(self, area):
        """
        Setter Method
        We use property name as a decorator here.
        """
        
        self.__area = area
        self.__radius = math.sqrt(self.__area)/3.14
```


```python
c = Circle(1)
print(f"{c.radius = }")  # difference in the way they were created as property
print(f"{c.area = }")
```

    c.radius = 1
    c.area = 3.14



```python
c.radius = 101

print(f"{c.radius = }")
print(f"{c.area = }")
```

    c.radius = 101
    c.area = 32031.14



```python
c.area = 3.14

print(f"{c.radius = }")
print(f"{c.area = }")
```

    c.radius = 0.5643326479831003
    c.area = 3.14



```python
class Celsius:
    def __init__(self, temperature = 0):
        self.temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32
```


```python
man = Celsius()
# set temperature
man.temperature = 37

# get temperature
print(man.temperature)


# get degrees Fahrenheit
print(man.to_fahrenheit())
##### print(Celsius.temperature)
```

    37
    98.60000000000001



```python
##############
### Riddle ###
##############
class MyClass(): 
    x = 0
    y = 100

a = MyClass()
b = MyClass()

a.x = 2
print(id(a.y), id(b.y))
print(id(a.x), id(b.x))
print(b.x)

MyClass.x = 4
print(a.x)
print(b.x)

MyClass.x = 7
print(a.x)
print(b.x)
print("~~~~~~")
b.x = MyClass.y
MyClass.x = 4
print(b.x)
```

    4450346896 4450346896
    4450343760 4450343696
    0
    2
    4
    2
    7
    ~~~~~~
    100


## Class with Getter and Setter


```python
class Celsius:
    def __init__(self, temperature = 0):
        self.set_temperature(temperature)

    def to_fahrenheit(self):
        return (self.get_temperature() * 1.8) + 32

    # new update
    def get_temperature(self):
        return self._temperature

    def set_temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        self._temperature = value
```

We can see above that new methods get_temperature() and set_temperature() were defined and furthermore, temperature was replaced with \_temperature. An underscore (\_) at the beginning is used to denote private variables in Python.

## Python Way - Property
----
The pythonic way to deal with the above problem is to use property. Here is how we could have achieved it.


```python
class Celsius:
    def __init__(self, temperature = 0):
        self.temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

    def get_temperature(self):
        print("Getting value")
        return self._temperature

    def set_temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value

    temperature = property(get_temperature, set_temperature)
```


```python
man = Celsius()
# set temperature
man.temperature = -10

# get temperature
print(man.temperature)

# get degrees Fahrenheit
print(man.to_fahrenheit())
##### print(Celsius.temperature)
```

    Setting value
    Setting value
    Getting value
    -10
    Getting value
    14.0



```python
man = Celsius()
# set temperature
man.temperature = -40

# get temperature
print(man.temperature)

# get degrees Fahrenheit
print(man.to_fahrenheit())
##### print(Celsius.temperature)
```

    Setting value
    Setting value
    Getting value
    -40
    Getting value
    -40.0


## Deep in Property

- Method 1
```python
temperature = property(get_temperature, set_temperature)
```

-  Method 2
```python
# make empty property
temprature = property()
# assign getter
temprature = temprature.getter(get_temperature)
# assign setter
temprature = temprature.setter(set_temperature)
```

- Method 3

```python
class Celsius:
    def __init__(self, temperature = 0):
        self._temperature = temperature

    def to_fahrenheit(self):
        return (self._temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value")
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value


celc = Celsius()
celc.temperature = 100
print(celc.temperature)
# del(celc.temperature) # Need to explicitly define a deleter
# print(celc.temperature)
```

Another example to 


```python
### Method 3
class Celsius:
    def __init__(self, temperature = 0):
        self._temperature = temperature

    def to_fahrenheit(self):
        return (self._temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value")
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value
        
    @temperature.deleter
    def temperature(self):
        print("deleting the property")
        del(self._temperature)
```


```python
celc = Celsius()
celc.temperature = 100
print(celc.temperature)
```

    Setting value
    Getting value
    100



```python

del(celc.temperature)
try:
    print(celc.temperature) # This property is no longer valid thus will error out
except Exception as e:
    print(e)
```

    deleting the property
    Getting value
    'Celsius' object has no attribute '_temperature'



```python
### Method 3
class Celsius:
    def __init__(self, temperature = 0):
        self._temperature = temperature

    def to_fahrenheit(self):
        return (self._temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value")
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value
        
#     @temperature.deleter
#     def temperature(self):
#         print("deleting the property")
#         del(self._temperature)
```


```python
# We need a deleter in order to be able to delete the property

celc = Celsius()
celc.temperature = 100
print(celc.temperature)

try:
    del(celc.temperature)
    print(celc.temperature) # This property is no longer valid thus will error out
except Exception as e:
    print(e)
```

    Setting value
    Getting value
    100
    property 'temperature' of 'Celsius' object has no deleter



```python
### Method 3
class Celsius:
    def __init__(self, temperature = 0):
        self._temperature = temperature

    def to_fahrenheit(self):
        return (self._temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value")
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        if value and value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value
        
    @temperature.deleter
    def temperature(self):
        print("deleting the property")
        del(self._temperature)
        self.temperature = None
#         del(self.temperature)
```


```python
# We need a deleter in order to be able to delete the property

celc = Celsius()
celc.temperature = 100
print(celc.temperature)

# try:
del(celc.temperature)
print(celc.temperature) # This property is no longer valid thus will error out
# except Exception as e:
#     print(e)
```

    Setting value
    Getting value
    100
    deleting the property
    Setting value
    Getting value
    None

