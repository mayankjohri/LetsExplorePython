# 1. at module level both locals and globals will return same values. 

a = 10

def test(a_par):
    print(a_par)
    a_par = "Pune Rocks"
    print('a_par1' in locals())
    print(globals())
    globals()['test'] = "testing"
    print(globals())

t = test
print(id(test))
test(a)
print("*"*20)
print(globals()['test'])
print(test)
print(id(test))
# test()
print(t)
print("*"*20)
print(globals()['test'])
