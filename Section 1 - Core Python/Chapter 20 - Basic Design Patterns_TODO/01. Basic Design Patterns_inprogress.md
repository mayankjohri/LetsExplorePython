# Basic Design Patterns_InProgress

- What is design Pattern


```python

```

- Principles of Design Pattern

- Program to an interface not an implementation
- Favour object composition over inheritance.


```python

```

## Creational Design Patterns

Creational Design Patterns as the names suggest handles creation of `classes` or `objects` by abstracting the creation of the classes.

| Pattern Name | Description |
|------|--------------|
|Singleton | a singleton with shared-state among instances|
|abstract_factory | use a generic function with specific factories|
|builder | instead of using multiple constructors, builder object receives parameters and returns constructed objects|
|factory| delegate a specialized function/method to create instances|
|lazy_evaluation| lazily-evaluated property pattern in Python|
|pool|preinstantiate and maintain a group of instances of the same type|
|prototype| use a factory and clones of a prototype for new instances (if instantiation is expensive)|

### Singleton

The main characteristics of a `Singleton` object is that:
- It can have at most one instance
- It should be globally accessible in the program

Although both of the above properties are important, but many times only one is implemented based on user requirement.

Lets implement the singleton design pattern on the example from `classes` chapter.


```python
from typing import Optional

class MetaSingleton(type):
    _instance : Optional[type] = None
    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return cls._instance

class Godlike(object, metaclass=MetaSingleton):  
    
    def __init__(self, name):
        """
        init function
        """
        print("Running init")
        self.name = name
    
    def print_name(self):
        print(self.name)
```


```python
g = Godlike("God")
d = Godlike("Welcome")

g.print_name()
d.print_name()
```

    Running init
    God
    God



```python

```

### abstract_factory


```python

```

### builder


```python

```


```python

```

### factory


```python

```

### lazy_evaluation


```python

```


```python

```


```python

```

### pool


```python

```

### prototype


```python

```


```python

```

Creational Patterns:
Pattern 	Description


Structural Patterns:
Pattern 	Description

3-tier 	data<->business logic<->presentation separation (strict relationships)
adapter 	adapt one interface to another using a white-list
bridge 	a client-provider middleman to soften interface changes
composite 	lets clients treat individual objects and compositions uniformly
decorator 	wrap functionality with other functionality in order to affect outputs
facade 	use one class as an API to a number of others
flyweight 	transparently reuse existing instances of objects with similar/identical state
front_controller 	single handler requests coming to the application
mvc 	model<->view<->controller (non-strict relationships)
proxy 	an object funnels operations to something else

Behavioral Patterns:
Pattern 	Description
chain_of_responsibility 	apply a chain of successive handlers to try and process the data
catalog 	general methods will call different specialized methods based on construction parameter
chaining_method 	continue callback next object method
command 	bundle a command and arguments to call later
iterator 	traverse a container and access the container's elements
iterator (alt. impl.) 	traverse a container and access the container's elements
mediator 	an object that knows how to connect other objects and act as a proxy
memento 	generate an opaque token that can be used to go back to a previous state
observer 	provide a callback for notification of events/changes to data
publish_subscribe 	a source syndicates events/data to 0+ registered listeners
registry 	keep track of all subclasses of a given class
specification 	business rules can be recombined by chaining the business rules together using boolean logic
state 	logic is organized into a discrete number of potential states and the next state that can be transitioned to
strategy 	selectable operations over the same data
template 	an object imposes a structure but takes pluggable components
visitor 	invoke a callback for all items of a collection

Design for Testability Patterns:
Pattern 	Description
dependency_injection 	3 variants of dependency injection

Fundamental Patterns:
Pattern 	Description
delegation_pattern 	an object handles a request by delegating to a second object (the delegate)

Others:
Pattern 	Description
blackboard 	architectural model, assemble different sub-system knowledge to build a solution, AI approach - non gang of four pattern
graph_search 	graphing algorithms - non gang of four pattern
hsm 	hierarchical state machine - non gang of four pattern

## Structurual Design Pattern

 - How my classes and objects are composed to form a larger structure


```python

```


```python

```


```python
def double(num):
    return num * 2

print(double(10))
```


```python
def execution_mapper(func):
    def inner(*args):
        print("Starting {}".format(func.__name__))
        print(func(*args))
        print("Ending {}".format(func.__name__))
    return inner

@execution_mapper
def double_new(num):
    
    return num * 2

double_new(120)
```

    Starting double_new
    240
    Ending double_new


#### `iterator`


```python
def tarverse(obj):
    for a in obj:
        yield a
    
name = "Mohan Shah"

d = tarverse(name)
for a in d:
    print(a, end=" ")
print()
    
names = ["Manish", "Rajeev", "Sachin"]
d = tarverse(names)
for a in d:
    print(a, end=" ")
```

    M o h a n   S h a h 
    Manish Rajeev Sachin 


```python

```


```python

```

#### References

- https://legacy.python.org/workshops/1997-10/proceedings/savikko.html
- https://python-patterns.guide/
- https://www.toptal.com/python/python-design-patterns
- https://github.com/faif/python-patterns
- https://stackabuse.com/design-patterns-in-python/
