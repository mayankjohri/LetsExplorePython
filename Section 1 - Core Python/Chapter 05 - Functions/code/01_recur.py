vals = [1, 2, 3, 4, 5, 6, 7]

def sum_it(a):
    if isinstance(a, list):
        print("before", a)
        val, a = a[0], a[1:]        
        print(val, a)
        if a:
            return val + sum_it(a)

    return val 

print(sum_it(vals))

