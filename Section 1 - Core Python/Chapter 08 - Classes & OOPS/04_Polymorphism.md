## Polymorphism

The polymorphism is the process of using an operator or function in different ways for different data input. In practical terms, polymorphism means that if class B inherits from class A, it doesn’t have to inherit everything about class A; it can do some of the things that class A does differently. (source: wikipedia)

Lets create a Class `Animal` which has a function `talk`. `Cat` and `Dog` inherits from class `Animal` 


```python
class Animal:
    def __init__(self, name=''):
        self.name = name

    def talk(self):
        pass
    
    def getHeadCount(self):
        return 1

class Cat(Animal):
    def talk(self):
        print ("Meow!")


class Dog(Animal):
    def talk(self):
        print ("Woof!")
```


```python
a = Animal()
a.talk()

cat = Cat("Missy")
cat.talk()
print(cat.getHeadCount())

dog = Dog("Rocky")
dog.talk()
print(dog.getHeadCount())
```

    Meow!
    1
    Woof!
    1



```python
class Gaddi(object):
    
    def engine_capacity(self):
        pass
    
    def get_engine_type(self):
        return "petrol"


class Zen_car(Gaddi):
    def engine_capacity(self):
        return "1000 cc"

class I10(Gaddi):
    def engine_capacity(self):
        return "1200 cc"

i10 = I10()
zen = Zen_car()
print(i10.engine_capacity())
print(zen.engine_capacity())
```

    1200 cc
    1000 cc


### Method Overloading

Officially **Python do not support method overloading** and trying to achieve will result in errors as shown in below examples.


```python
class Cars(object):
    def seats(self, seat_type):
        print("seat_type:", seat_type)
        
    def seats(self, seat_type, count):
        print("seat_type:", seat_type)
        print("count:", count)
        
car = Cars()
try:
    car.seats("leather")
except Exception as e:
    print("Error:", e)
```

    Error: Cars.seats() missing 1 required positional argument: 'count'


In the above example, we had two functions with same name and different numbers of arguments, Python only use the last defined function and previous one is discarded.


```python
class Cars(object):
    def seats(self, seat_type):
        print("seat_type:", seat_type)
        
class RaceCar(Cars):
    def seats(self, seat_type, count):
        print("seat_type:", seat_type)
        print("count:", count)
        
car = RaceCar()
try:
    car.seats("leather")
except Exception as e:
    print("Error:", e)
```

    Error: seats() missing 1 required positional argument: 'count'


In the above example, we had two functions with same name and different numbers of arguments one in `RaceCar` and another one in its parent class `Cars`, and when we call the function `seats` from an object of `RaceCar` it rightly calls `seats` from `RaceCar` instead of from `Cars`.

Now if you have a scenario like that and then one of the solution which we can do is use the following process. 


```python
class Cars(object):
    def seats(self, seat_type):
        print("seat_type:", seat_type)
        
class RaceCar(Cars):
    def seats(self, seat_type, count=None):
        if count:
            print("seat_type:", seat_type)
            print("count:", count)
        else:
            super().seats(seat_type)

car = RaceCar()
try:
    # This will call parent seats 
    car.seats(10, "leather")
    # This will call child seats
    print("*"*10)
    car.seats(10)
except Exception as e:
    print("Error:", e)
```

    seat_type: 10
    count: leather
    **********
    seat_type: 10



```python
# Using default arguments 

class RaceCar(object):
    def _seats(self, seat_type):
        print(f"Inside _seats: {seat_type}")
    
    def seats(self, seat_type, count=None):
        if count is None:
            self._seats(seat_type)
        else:
            print("seat_type:", seat_type)
            print("count:", count)

car = RaceCar()
try:
    # This will call parent seats 
    car.seats(10, "leather")
    # This will call child seats
    print("*"*10)
    car.seats(10)
except Exception as e:
    print("Error:", e)
```

    seat_type: 10
    count: leather
    **********
    Inside _seats: 10



```python
# Using default arguments 

class RaceCar(object):
    def _seats(self, seat_type):
        print(f"Inside _seats: {seat_type}")
    
    def seats(self, seat_type, count=None):
        if not count:
            self._seats(seat_type)
        else:
            print("seat_type:", seat_type)
            print("count:", count)

car = RaceCar()
try:
    # This will call parent seats 
    car.seats(10, "leather")
    # This will call child seats
    print("*"*10)
    car.seats(10)
except Exception as e:
    print("Error:", e)
```

    seat_type: 10
    count: leather
    **********
    Inside _seats: 10

