## `dataclasses` - Work In Progress

`dataclasses` provides a way to create classes whose main objective is only or mostly to store data. That is, they will primarily be used to store a set of data in logical group, which should be accessed using its Attributes. 

Lets take an example, We are in need to mechanism to store a point in space {and not space-time ;)}. For that we need only three data points to be stored, i.e. `x`, `y` and `z` cordinates.


```python
from dataclasses import dataclass

@dataclass
class Point:
    x: float 
    y: float = 0.0  # Setting default values
    z: float = 0.0  # Setting default values
```


```python
pZero = Point(0)
print(f"{pZero = }")
print(f"{dir(pZero) = }")
```

    pZero = Point(x=0, y=0.0, z=0.0)
    dir(pZero) = ['__annotations__', '__class__', '__dataclass_fields__', '__dataclass_params__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__match_args__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'x', 'y', 'z']


We can also create dataclass as shown below


```python
from dataclasses import make_dataclass

DynamicPoint = make_dataclass("Point", ("x", "y", "z"))

dp = DynamicPoint(1, 2, 3)
```


```python
print(dp)
print(dir(dp))
```

    Point(x=1, y=2, z=3)
    ['__annotations__', '__class__', '__dataclass_fields__', '__dataclass_params__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__match_args__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'x', 'y', 'z']


as you can see in both the above cases, `__init__` function was automatically created.  

Also following attributes are already created `__eq__`,  `__ge__`, `__gt__`, `__le__`, `__lt__`, `__ne__`, `__repr__`, but some of them have not been implemented as shown below


```python
print(dp.__ge__(pZero))
```

    NotImplemented


thus when we try to use them we get 


```python
try:
    print(dp >= pZero)
except TypeError as te:
    print(te)
```

    '>=' not supported between instances of 'Point' and 'Point'


and only `__eq__`, `__ne__` and `__repr__` have been implemented by default as shown below


```python
print(dp.__repr__())
print(dp)
```

    Point(x=1, y=2, z=3)
    Point(x=1, y=2, z=3)



```python
dp == pZero
```




    False




```python
dp != pZero
```




    True




```python
dp2 = DynamicPoint(1, 2, 3)

# In normal objects as __eq__ is not implemented this would return `False`
dp == dp2 
```




    True



Above code failed because we have not implemented any of the functions. `@dataclass(init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False)`

## dataclass decorator options

`dataclass` decorator provides few options as shown below with their default values. In this section we are going to see the effects of them on the resultant data.


```python
@dataclass(init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False)
class Point:
    x: float 
    y: float = 0.0  # Setting default values
    z: float = 0.0  # Setting default values
```

#### Effects of `Order=True`

`Order=True` options is responsible of automatically implementing the `__ge__`, `__gt__`, `__le__`, `__lt__` functions as shown in the below example.


```python
@dataclass(order=True)
class Point:
    x: float 
    y: float = 0.0  # Setting default values
    z: float = 0.0  # Setting default values
```


```python
pOne = Point(1, 2, 1)
print(f"{pOne=}")

pTwo = Point(1, 5, 1)
print(f"{pTwo=}")
```

    pOne=Point(x=1, y=2, z=1)
    pTwo=Point(x=1, y=5, z=1)



```python
print(f"{pOne > pTwo=}")
print(f"{pOne < pTwo=}")
print(f"{pOne >= pTwo=}")
print(f"{pOne <= pTwo=}")
print(f"{pOne == pTwo=}")
```

    pOne > pTwo=False
    pOne < pTwo=True
    pOne >= pTwo=False
    pOne <= pTwo=True
    pOne == pTwo=False


#### Effects of `unsafe_hash=False`

Effect of `unsafe_hash=False` depends on the values of `eq` and `frozen`

|   unsafe_hash  |  eq   | frozen |  Effect                       |
|----------------|-------|--------|-------------------------------|
| `False`        |`False`|`False` |                               |
| `False`        |`False`|`True`  |                               |
| `False`        |`True` |`False` | `__hash__()` will be set to None|
| `False`        |`True` |`True`  | `__hash__()` will generate    |
| `True`         |`False`|`False` |                               |
| `True`         |`False`|`True`  |                               |
| `True`         |`True` |`False` |                               |
| `True`         |`True` |`True`  | `__hash__()` will generate    |


```python
@dataclass(eq=False, unsafe_hash=False, frozen=False)
class Point:
    x: float 
    y: float = 0.0  # Setting default values
    z: float = 0.0  # Setting default values
        
p = Point(1)
print(p)
print(dir(p))
p.__hash__()
```

    Point(x=1, y=0.0, z=0.0)
    ['__annotations__', '__class__', '__dataclass_fields__', '__dataclass_params__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'x', 'y', 'z']





    8761049966686




```python
@dataclass(unsafe_hash=True, eq=True, frozen=False)
class Point:
    x: float 
    y: float = 0.0  # Setting default values
    z: float = 0.0  # Setting default values
        
p = Point(1)
print(p)
print(dir(p))
print(p.__hash__() == None)
# p.x = 12
# print(p)
```

    Point(x=1, y=0.0, z=0.0)
    ['__annotations__', '__class__', '__dataclass_fields__', '__dataclass_params__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'x', 'y', 'z']
    False


### Field objects


`name`: The name of the field.
`type`: The type of the field.

`default`, `default_factory`, `init`, `repr`, `hash`, `compare`, and `metadata` have the identical meaning and values as they do in the `field()` declaration.


## `dataclasses` vs `namedtuple` 

|  Property  |  dataclasses  | namedtupe   |
|:-----------:|:------------:|:-----------:|
| Immutable  |  No           | yes         |
| Data Access | attributes   | attributes  |
| Functions   | Yes          | No          |
| Initializer | SortOf       | No          |

## Summary

|  Feature       | Keyword       | Example                         | Implement in a Class       |
|----------------|---------------|---------------------------------|----------------------------|
| Attributes     |  `init`       |  `Color().r -> 0`               |  `__init__`                |
| Representation |  `repr`       |`Color() -> Color(r=0, g=0, b=0)`|  `__repr__`                |
| Comparision*   |  `eq`         |`Color() == Color(0, 0, 0) -> True` |  `__eq__`                                                  |
| Order                |  order               |  `sorted([Color(0, 50, 0), Color()]) -> ...`         | `__lt__`, `__le__`, `__gt__`, `__ge__`         |
| Hashable             |  `unsafe_hash`/`frozen`  |  `{Color(), {Color()}} -> {Color(r=0, g=0, b=0)}`    |  `__hash__`                               |
| Immutable            |  `frozen + eq`         |  `Color().r = 10 -> TypeError`                       |  `__setattr__`, `__delattr__`               |
|                      |                      |                                                    |                                         |
| Unpacking+           | `-`                   |  `r, g, b = Color()`                                 |   `__iter__`                              |
| Optimization+        |  -                   |  `sys.getsizeof(SlottedColor) -> 888`                |  `__slots__ `                             |

