# `_`, `__` and `__attr__` in Python

Certain classes of identifiers (besides keywords) have special meanings. These classes are identified by the patterns of leading and trailing underscore characters:

`_*`
Not imported by "from module import *". The special identifier "`_`" is used in the interactive interpreter to store the result of the last evaluation; it is stored in the `__builtin__` module. When not in interactive mode, "`_`" has no special meaning and is not defined. See section 6.12, ``The import statement.''
Note: The name "_" is often used in conjunction with internationalization; refer to the documentation for the gettext module for more information on this convention.

`__*__`
System-defined names. These names are defined by the interpreter and its implementation (including the standard library); applications should not expect to define additional names using this convention. The set of names of this class defined by Python may be extended in future versions. See section 3.4, ``Special method names.''

`__*`
Class-private names. Names in this category, when used within the context of a class definition, are re-written to use a mangled form to help avoid name clashes between **private** attributes of **base** and **derived** classes.


```python
# Static function
class SumTotal(object):
    init_no = [10]
    
    def __init__(self):
        self.actual = 100
        pass
    
    @staticmethod
    def static_method(nums):
        # They have no access to object/class attributes
        print(nums)
    
    @classmethod
    def cls_method(cls):
        print(cls.init_no)
        # As class methods do not have access to object attributes
        try:
            print(cls.actual)
        except Exception as e:
            print(e)
        

st = SumTotal()
st.static_method(1010)

st.cls_method()
SumTotal.static_method(22)
```

    1010
    [10]
    type object 'SumTotal' has no attribute 'actual'
    22



```python
# Explicitly calling parent function

class Base(object):
    def test(self, data):
        print("data: {}".format(data))
        
class Child(Base):
    def test(self, data):
        super().test(data)
        
c = Child()
c.test("testing")
```

    data: testing


### `__slots__`

Using `__slots__` we can define which attributes can be created for the class.


```python
class User(object):

    __slots__ = ['first_name', 'last_name']

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


roshan = User("Roshan", "Garg")
print(roshan.first_name)

try:
    roshan.impossible = "Should Fail"
except Exception as e:
    print(f"{e = }")

```

    Roshan
    e = AttributeError("'User' object has no attribute 'impossible'")



```python
class User(object):

    __slots__ = ['first_name', 'last_name']

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


roshan = User("Roshan", "Garg")
print(roshan.first_name)
roshan.last_name = "Musheer"

```

    Roshan



```python
class User(object):
    
    # All the object attributes should be listed in 
    # `__slots__`
    __slots__ = [ 'first_name', 'last_name']
    def __get_name(self):
        return f"{first_name} {last_name}"
    
    def __set_name(self, name):
        self.first_name, self.last_name = split(name)
        
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.name = property(self.__get_name, self.__set_name)
    
    
try:
    roshan = User("Roshan", "Garg")
    
#     print(roshan.first_name)
#     roshan.last_name = "Musheer"
#     roshan.impossible = "Should Fail"
except Exception as e:
    print(f"{e = }")

```

    e = AttributeError("'User' object has no attribute 'name'")


### Monkey Patching of Classes

Below is a very simple example of monkey patching of classes in Python. 
> **Note**
> ____
>After patching, all the object created will get the updates irrespective of when they were created as shown in the below example. 


```python
# mp/01.py
class Hanuman(object):
    pass

def chant(cls):
    print("Jai Jai Shri Ram")

heman = Hanuman()
print('chant' in heman.__dir__())

Hanuman.chant = chant

hanuman = Hanuman()
hanuman.chant()

print('chant' in heman.__dir__())
print('chant' in hanuman.__dir__())
```

    False
    Jai Jai Shri Ram
    True
    True



```python
## Patching a new variable
# mp/02.py

class Hanuman(object):
    def chant(self):
        print(" __ Jai Jai Shri Ram __")

def chant(cls):
    print("Jai Jai Shri Ram")

heman = Hanuman()
heman.chant()

Hanuman.chant = chant

hanuman = Hanuman()
hanuman.chant()
heman.chant()
```

     __ Jai Jai Shri Ram __
    Jai Jai Shri Ram
    Jai Jai Shri Ram



```python
## Patching existing function
# mp/03.py
class Hanuman(object):
    def chant(self, padding="****"):
        print(" __ Jai Jai Shri Ram __")
        print(padding)

def chant(cls):
    print("Jai Jai Shri Ram", cls.padding)

heman = Hanuman()
heman.chant()
print('padding' in heman.__dir__())

Hanuman.chant = chant
hanuman = Hanuman()
Hanuman.padding = "$$$"

hanuman.chant()
heman.chant()
print('padding' in heman.__dir__())
print('padding' in hanuman.__dir__())
```

     __ Jai Jai Shri Ram __
    ****
    False
    Jai Jai Shri Ram $$$
    Jai Jai Shri Ram $$$
    True
    True


### Effects of patching Parent Class on child objects


```python
# mp/04.py
class Ram(object):
    pass

class Kush(Ram):
    pass

def chant(cls):
    print("... Ohm ...")
    
kush = Kush()
print('chant' in kush.__dir__())

Ram.chant = chant
love = Kush()

love.chant()
kush.chant()
print('chant' in kush.__dir__())
print('chant' in love.__dir__())
```

    False
    ... Ohm ...
    ... Ohm ...
    True
    True


### `match/case` and Objects


```python
from dataclasses import dataclass

@dataclass
class User:
    first_name: str
    last_name: str
```


```python
mayank = User(first_name="Mayank", last_name="Johri")

match mayank:
    case User(first_name="Mayank", last_name="Johri"):
        print(f"Received: {mayank = }")
    
    case User(first_name="mayank", last_name="Johri"):
        print(f"Good Message Received: {mayank = }")
        
    case (False, msg):
        print(f"Bad Message Received: {msg}")
    
    case _:
        print(f"{mayank = }")
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-48-4a30930c6505> in <module>
    ----> 1 mayank = User(first_name="Mayank", last_name="Johri")
          2 
          3 match mayank:
          4     case User(first_name="Mayank", last_name="Johri"):
          5         print(f"Received: {mayank = }")


    TypeError: User.__init__() missing 1 required positional argument: 'title'



```python
class User:
    first_name: str
    last_name: str
```


```python
mayank = User()
mayank.first_name = "mayank"
mayank.last_name = "Johri"

match mayank:
    case User(first_name="Mayank", last_name="Johri"):
        print("Default True message received")
    
    case User(first_name="mayank", last_name="Johri"):
        print(f"Good Message Received: {mayank = }")
        
    case (False, msg):
        print(f"Bad Message Received: {msg}")
    
    case _:
        print(f"{mayank = }")
```

    Good Message Received: mayank = <__main__.User object at 0x7fe215db3880>



```python
mayank = User()
mayank.first_name = "mayank"
mayank.last_name = "Johri"

match mayank:
    case User(first_name="Mayank", last_name="Johri"):
        print("Default True message received")
    
    case User(last_name="Johri", first_name="mayank"):
        print(f"Good Message Received 1: {mayank = }")
    
    case User(first_name="mayank", last_name="Johri"):
        print(f"Good Message Received 2: {mayank = }")
        
    case (False, msg):
        print(f"Bad Message Received: {msg}")
    
    case _:
        print(f"{mayank = }")
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-47-89715bf74237> in <module>
    ----> 1 mayank = User()
          2 mayank.first_name = "mayank"
          3 mayank.last_name = "Johri"
          4 
          5 match mayank:


    TypeError: User.__init__() missing 3 required positional arguments: 'title', 'first_name', and 'last_name'


We can create our own custom order of attributes as shown in the example below


```python
class User:
    title: str
    first_name: str
    last_name: str
        
    __match_args__ = ("last_name", "first_name")
    def __init__(self, title: str, first_name: str, last_name: str):
        self.title = title
        self.first_name = first_name
        self.last_name = last_name
    
    def __repr__(self):
        return f"{self.title} {self.first_name} {self.last_name}"
```


```python
mayank = User("Mr", "mayank", "Johri")

match mayank:
    case User(first_name="Mayank", last_name="Johri"):
        print("Default True message received")
    
    case User( last_name="Johri", first_name="mayank"):
        print(f"Good Message Received 1: {mayank = }")
    
    case User(first_name="mayank", last_name="Johri"):
        print(f"Good Message Received 2: {mayank = }")
        
    case (False, msg):
        print(f"Bad Message Received: {msg}")
    
    case _:
        print(f"{mayank = }")
```

    Good Message Received 1: mayank = Mr mayank Johri



```python
mayank = User("DD", "mayank", "Johri")

match mayank:
    case User(first_name="Mayank", last_name="Johri"):
        print("Default True message received")
        
    case User( last_name="Johri", first_name="mayank"):
        print(f"Good Message Received 1: {mayank = }")
    
    case User(first_name="mayank", last_name="Johri"):
        print(f"Good Message Received 2: {mayank = }")
    
    case (False, msg):
        print(f"Bad Message Received: {msg}")
    
    case _:
        print(f"{mayank = }")
```

    Good Message Received 1: mayank = DD mayank Johri


### `Enum` and `match`


```python
from enum import Enum

class Status(Enum):
    RUNNING = "0"
    PENDING = 1
    COMPLETED = "Done"
    
data = Status.RUNNING


match data:
    case Status.RUNNING:
        print("Task is running")
    case Status.PENDING:
        print("Task is pending")
    case Status.COMPLETED:
        print("Task is COMPLETED")
```

    Task is running

